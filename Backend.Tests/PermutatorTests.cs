﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Backend.Tests
{
    [TestFixture]
    public class PermutatorTests
    {
        [Test]
        public void FirstElementsAreDifferent()
        {
            var els = Enumerable.Range(1, 5).ToList();
            var elsEntrances = new Dictionary<int, int>();
            foreach (var el in els)
                elsEntrances[el] = 0;

            for (var i = 0; i < els.Count * els.Count; i++)
            {
                var elsPermuted = Permutator.Permute(els);
                var elsPermutedEnumerator = elsPermuted.GetEnumerator();
                elsPermutedEnumerator.MoveNext();
                elsEntrances[(int) elsPermutedEnumerator.Current]++;
            }

            foreach (var elsEntranceCount in elsEntrances.Values)
                Assert.IsTrue(elsEntranceCount > 0);
        }

        [Test]
        public void SequencesAreDifferent()
        {
            var lastSequence = Enumerable.Range(1, 5).ToList();

            var matchTimes = 0;
            for (var i = 0; i < lastSequence.Count; i++)
            {
                var elsPermuted = Permutator.Permute(lastSequence).Cast<int>().ToList();
                matchTimes += lastSequence.Where((t, j) => elsPermuted[j] == t).Count();

                lastSequence = elsPermuted;
            }

            // Match < 75%
            Assert.True(matchTimes / Math.Pow(lastSequence.Count, 2) < 0.75);
        }
    }
}