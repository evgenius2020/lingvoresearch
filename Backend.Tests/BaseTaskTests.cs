﻿using Backend.Tasks.SelectRightAnswer;
using NUnit.Framework;

namespace Backend.Tests
{
    [TestFixture]
    public class BaseTaskTests
    {
        [Test]
        public void OptionIsCorrectTest()
        {
            var task = new SelectRightAnswerWithTranslateTask();
            
            var hasRightOption = false;
            foreach (var taskRoundOption in task.RoundOptions)
                hasRightOption |= taskRoundOption == task.RightOption;
            Assert.IsTrue(hasRightOption);
            
            foreach (var taskRoundOption in task.RoundOptions)
                if (taskRoundOption == task.RightOption)
                    Assert.IsTrue(task.OptionIsCorrect(taskRoundOption));
        }

        [Test]
        public void NextRoundTest()
        {
            var task = new SelectRightAnswerTask();
            Assert.AreEqual(task.TotalRoundsNumber, SelectRightAnswerTask.RoundInfos.Length);
            
            Assert.AreEqual(1, task.CurrentRoundNumber);
            task.NextRound();
            Assert.AreEqual(2, task.CurrentRoundNumber);
            
            for (int i = 0; i < task.TotalRoundsNumber * 2; i++)
                task.NextRound();
            Assert.IsTrue(task.IsComplete);
        }
    }
}