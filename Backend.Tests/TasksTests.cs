﻿using System.Linq;
using System.Text.RegularExpressions;
using Backend.Tasks.CollectPhrase;
using Backend.Tasks.CollectSentence;
using Backend.Tasks.SelectRightAnswer;
using NUnit.Framework;

namespace Backend.Tests
{
    [TestFixture]
    public class TasksTests
    {
        [Test]
        public void RoundsAreCorrect_CollectSentenceTask()
        {
            foreach (var roundInfo in CollectSentenceTask.RoundInfos)
            {
                Assert.AreEqual(roundInfo.RightOption.Split(' ').Length, roundInfo.Options.Length);
                Assert.AreEqual(roundInfo.RightOption, string.Join(" ", roundInfo.Options));
                Assert.AreEqual(Regex.Replace(roundInfo.Phrase, "[-.?!)(,:]", "").ToLower(), roundInfo.RightOption);
            }
        }

        [Test]
        public void RoundsAreCorrect_CollectWordTask()
        {
            foreach (var roundInfo in CollectWordTask.RoundInfos)
            {
                Assert.AreEqual(roundInfo.Phrase.Length, roundInfo.Options.Length);
                Assert.AreEqual(roundInfo.Phrase.ToLower(), roundInfo.RightOption);
                Assert.AreEqual(roundInfo.RightOption, string.Join("", roundInfo.Options));
            }
        }

        [Test]
        public void RoundsAreCorrect_IsCompleted()
        {
            var task = new SelectRightAnswerWithTranslateTask();
            while (task.CurrentRoundNumber <= task.TotalRoundsNumber) task.NextRound();
            Assert.IsTrue(task.IsComplete);
        }

        [Test]
        public void RoundsAreCorrect_IsOptionCorrect()
        {
            var task = new SelectRightAnswerWithTranslateTask();
            foreach (var roundInfo in TypeWordTask.RoundInfos) Assert.IsTrue(task.OptionIsCorrect(task.RightOption));
        }

        [Test]
        public void RoundsAreCorrect_SelectRightAnswerTask()
        {
            foreach (var roundInfo in SelectRightAnswerTask.RoundInfos)
            {
                Assert.AreEqual(4, roundInfo.Options.Length);
                Assert.AreEqual(4, roundInfo.Options.Distinct().Count());
                Assert.IsTrue(roundInfo.Options.Contains(roundInfo.RightOption));
                Assert.IsTrue(roundInfo.Phrase.Contains('…'));
                Assert.AreEqual(2, roundInfo.Phrase.Split('…').Length);
            }
        }

        [Test]
        public void RoundsAreCorrect_SelectRightAnswerWithTranslateTask()
        {
            foreach (var roundInfo in SelectRightAnswerWithTranslateTask.RoundInfos)
            {
                Assert.AreEqual(4, roundInfo.Options.Length);
                Assert.AreEqual(4, roundInfo.Options.Distinct().Count());
                Assert.IsTrue(roundInfo.Options.Contains(roundInfo.RightOption));
                Assert.IsTrue(roundInfo.Phrase.Contains('…'));
                Assert.AreEqual(2, roundInfo.Phrase.Split('…').Length);
            }
        }

        [Test]
        public void RoundsAreCorrect_TranslateRussianToGermanTask_TranslateGermanToRussianTask()
        {
            foreach (var roundInfo in
                TranslateRussianToGermanTask.RoundInfos.Concat(TranslateGermanToRussianTask.RoundInfos))
            {
                Assert.AreEqual(4, roundInfo.Options.Length);
                Assert.AreEqual(4, roundInfo.Options.Distinct().Count());
                Assert.IsTrue(roundInfo.Options.Contains(roundInfo.RightOption));
            }
        }

        [Test]
        public void RoundsAreCorrect_TypeWordTask()
        {
            foreach (var roundInfo in TypeWordTask.RoundInfos)
            {
                Assert.AreEqual(roundInfo.Phrase.Length, roundInfo.Options.Length);
                Assert.AreEqual(roundInfo.RightOption, string.Join("", roundInfo.Options));
                Assert.AreEqual(roundInfo.Phrase.ToLower(), roundInfo.RightOption);
            }
        }

        [Test]
        public void RoundsNumbersAre30()
        {
            Assert.AreEqual(30, SelectRightAnswerTask.RoundInfos.Length);
            Assert.AreEqual(30, TranslateRussianToGermanTask.RoundInfos.Length);
            Assert.AreEqual(30, TranslateGermanToRussianTask.RoundInfos.Length);
            Assert.AreEqual(30, CollectWordTask.RoundInfos.Length);
            Assert.AreEqual(30, TypeWordTask.RoundInfos.Length);
            Assert.AreEqual(30, SelectRightAnswerWithTranslateTask.RoundInfos.Length);
            Assert.AreEqual(30, CollectSentenceTask.RoundInfos.Length);
        }
    }
}