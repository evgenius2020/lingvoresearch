using System;
using System.IO;
using System.Text;
using Backend.Tasks.CollectPhrase;
using Backend.Tasks.CollectSentence;
using Backend.Tasks.SelectRightAnswer;
using NUnit.Framework;

namespace Backend.Tests
{
    internal class ProtocolWriterTests
    {
        private void ExecuteMethod(Action<ProtocolWriter> actions, string expected_str)
        {
            using (var stream = new MemoryStream())
            using (var writer = new StreamWriter(stream))
            {
                var protocolWriter = new ProtocolWriter(writer);
                actions(protocolWriter);
                var actual = Encoding.UTF8.GetString(stream.ToArray());
                Assert.AreEqual(expected_str, actual);
            }
        }

        [Test]
        public void AppendParticipantInfoTest()
        {
            ExecuteMethod(
                protocolWriter => protocolWriter.AppendParticipantInfo("admin", 12),
                "Имя: admin\nВозраст: 12\r\n");
        }

        [Test]
        public void AppendExperimentStartTest()
        {
            ExecuteMethod(
                protocolWriter => protocolWriter.AppendExperimentStart(DateTime.MinValue, "2.0"),
                $"\n===================\r\nНачало эксперимента\r\nLingvoResearch v2.0\r\n{DateTime.MinValue.ToString()}\r\n"
            );
        }

        [Test]
        public void AppendExperimentEndTest()
        {
            ExecuteMethod(
                protocolWriter => protocolWriter.AppendExperimentEnd(),
                "Конец эксперимента\r\n");
        }

        [Test]
        public void AppendTaskInfoTest()
        {
            var task = new SelectRightAnswerTask();
            ExecuteMethod(
                protocolWriter => protocolWriter.AppendTaskInfo(task),
                "\nЗадание #1\n" +
                "Название: Выберите правильный ответ\n" +
                "Описание: Перетащите мышкой пропущенное слово\r\n");
        }

        [Test]
        public void AppendTaskInfoTest_TranslateRussianToGerman()
        {
            var task = new TranslateRussianToGermanTask();
            ExecuteMethod(
                protocolWriter => protocolWriter.AppendTaskInfo(task),
                "\nЗадание #2\n" +
                "Название: Выберите правильный перевод (рус-нем)\n" +
                "Описание: Нажмите мышкой нужное слово\r\n");
        }

        [Test]
        public void AppendTaskInfoTest_TranslateGermanToRussian()
        {
            var task = new TranslateGermanToRussianTask();
            ExecuteMethod(
                protocolWriter => protocolWriter.AppendTaskInfo(task),
                "\nЗадание #3\n" +
                "Название: Выберите правильный перевод (нем-рус)\n" +
                "Описание: Нажмите мышкой нужное слово\r\n");
        }

        [Test]
        public void AppendTaskInfoTest_CollectWord()
        {
            var task = new CollectWordTask();
            ExecuteMethod(
                protocolWriter => protocolWriter.AppendTaskInfo(task),
                "\nЗадание #4\n" +
                "Название: Соберите слово\n" +
                "Описание: Нажимайте для этого буквы в правильном порядке\r\n");
        }

        [Test]
        public void AppendTaskInfoTest_TypeWord()
        {
            var task = new TypeWordTask();
            ExecuteMethod(
                protocolWriter => protocolWriter.AppendTaskInfo(task),
                "\nЗадание #5\n" +
                "Название: Напечатай слово\n" +
                "Описание: Напечатай озвученное слово с помощью клавиатуры\r\n");
        }

        [Test]
        public void AppendTaskInfoTest_SelectRightAnswerWithTranslate()
        {
            var task = new SelectRightAnswerWithTranslateTask();
            ExecuteMethod(
                protocolWriter => protocolWriter.AppendTaskInfo(task),
                "\nЗадание #6\n" +
                "Название: Вставить пропуск с переводом\n" +
                "Описание: Заполните пропуск при помощи перевода\r\n");
        }

        [Test]
        public void AppendTaskInfoTest_CollectSentence()
        {
            var task = new CollectSentenceTask();
            ExecuteMethod(
                protocolWriter => protocolWriter.AppendTaskInfo(task),
                "\nЗадание #7\n" +
                "Название: Перевод на немецкий\n" +
                "Описание: Расположите слова в правильном порядке при помощи перевода\r\n");
        }

        [Test]
        public void AppendTaskEndTest()
        {
            ExecuteMethod(
                protocolWriter => protocolWriter.AppendTaskEnd(9),
                "Задание завершено за 9 мс.\r\n");
        }

        [Test]
        public void AppendRoundInfoTest()
        {
            var task = new SelectRightAnswerTask();
            ExecuteMethod(
                protocolWriter => protocolWriter.AppendRoundInfo(task),
                "\n= Раунд #1\n" +
                $"= Фраза: {task.RoundPhrase}\n" +
                $"= Варианты: ['{task.RoundOptions[0]}', '{task.RoundOptions[1]}', " +
                $"'{task.RoundOptions[2]}', '{task.RoundOptions[3]}']\n" +
                $"= Правильный ответ: {task.RightOption}\r\n");
        }

        [Test]
        public void AppendRoundOptionSelectTest()
        {
            ExecuteMethod(
                protocolWriter => protocolWriter.AppendRoundOptionSelect("test", 8),
                "=== Выбран вариант 'test' за 8 мс.\r\n");
        }
    }
}