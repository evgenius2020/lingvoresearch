﻿using System;
using System.Windows;
using Frontend.Pages;
using Frontend.Pages.Tasks;

namespace Frontend
{
    partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            var settings = new Settings();
            var settingsPage = new SettingsPage(settings);

            var startPage = new StartPage();
            var tasksPage = new TasksPage(settings);
            var finishPage = new FinishPage();

            Title = $"LingvoResearch v{Settings.Version}";

            settingsPage.StartPressed += (sender, args) =>
            {
                startPage.InitIntoText((bool)settingsPage.StartNewTaskCheckBox.IsChecked);
                TasksFrame.Navigate(startPage);
                WindowStyle = WindowStyle.None;
                WindowState = WindowState.Maximized;
            };
            startPage.StartPressed += (_, _) =>
            {
                settings.StimTracker?.SendStimCode(Settings.ExperimentStartStimTrackerLabel);
                settings.SplitOptionsStimTracker?.SendStimCode(Settings.SplitOptionsExperimentStartStimTrackerLabel);
                TasksFrame.Navigate(tasksPage);
                tasksPage.Start((bool)settingsPage.StartNewTaskCheckBox.IsChecked);
            };
            tasksPage.TasksFinished += (_, _) =>
            {
                settings.StimTracker?.SendStimCode(Settings.ExperimentEndStimTrackerLabel);
                settings.SplitOptionsStimTracker?.SendStimCode(Settings.SplitOptionsExperimentEndStimTrackerLabel);
                TasksFrame.Navigate(finishPage);
            };
            finishPage.FinishPressed += (_, _) => Close();

            TasksFrame.Navigate(settingsPage);

            Closing += (sender, args) =>
            {
                settings.StimTracker?.CloseConnection();
                settings.SplitOptionsStimTracker?.CloseConnection();
            };
        }
    }
}