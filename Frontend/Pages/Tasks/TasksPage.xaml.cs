﻿using System;
using Backend;
using Backend.Tasks;
using Frontend.Pages.Tasks.TaskPages;

namespace Frontend.Pages.Tasks
{
    public partial class TasksPage
    {
        private readonly Settings _settings;
        private readonly Timer _timer;

        public TasksPage(Settings settings)
        {
            InitializeComponent();
            _settings = settings;
            _timer = new Timer();
        }

        private ProtocolWriter ProtocolWriter => _settings.ProtocolWriter;
        private CsvProtocolWriter CsvProtocolWriter => _settings.CsvProtocolWriter;
        private StimTracker StimTracker => _settings.StimTracker;
        private StimTracker SplitOptionsStimTracker => _settings.SplitOptionsStimTracker;
        public event EventHandler TasksFinished;

        public void Start(bool startNewTask)
        {
            ProtocolWriter?.AppendExperimentStart(DateTime.Now, Settings.Version);
            ProtocolWriter?.AppendParticipantInfo(_settings.ParticipantName, _settings.ParticipantAge);
            CsvProtocolWriter?.WriteExperimentInfo(Settings.Version, Settings.ExperimentStartTime,
                _settings.ParticipantName,
                _settings.ParticipantAge);
            CsvProtocolWriter?.WriteResponseHeader();
            ChangeCurrentTaskPage(startNewTask ? 10 : 1);
        }

        private BaseTaskPage CreateTaskPage(int taskId)
        {
            return taskId switch
            {
                1 => new SelectRightAnswerTaskPage(),
                2 => new TranslateTaskPage(TranslateTaskPageLanguage.RussianToGerman),
                3 => new TranslateTaskPage(TranslateTaskPageLanguage.GermanToRussian),
                4 => new CollectingTaskPage(),
                5 => new TypeWordTaskPage(),
                6 => new SelectRightAnswerTaskPage(true),
                7 => new CollectingTaskPage(true),
                10 => new SplitOptionsTaskPage(SplitOptionsStimTracker),
                _ => throw new ArgumentOutOfRangeException(nameof(taskId), taskId, null)
            };
        }

        private void RoundStart(BaseTask task)
        {
            UpdateTaskNameLabel(task);
            _timer.RestartRoundTimer();
            StimTracker?.SendStimCode(Settings.RoundStartStimTrackerLabel);
            SplitOptionsStimTracker?.SendStimCode(Settings.SplitOptionsRoundStartStimTrackerLabel);
            ProtocolWriter?.AppendRoundInfo(task);
        }

        private void RoundEnd()
        {
            StimTracker?.SendStimCode(Settings.RoundEndStimTrackerLabel);
            SplitOptionsStimTracker?.SendStimCode(Settings.SplitOptionsRoundEndStimTrackerLabel);
            ProtocolWriter?.AppendRoundEnd(_timer.GetRoundTimerElapsed());
        }

        private void RoundAttempt(int attemptNumber, int totalAttempts)
        {
            StimTracker?.SendStimCode(Settings.RoundAttemptStimTrackerLabel);
            ProtocolWriter?.AppendRoundAttempt(
                attemptNumber, totalAttempts, _timer.GetRoundTimerElapsed());
        }

        private void RoundSkip() => ProtocolWriter?.AppendRoundSkip();

        private void ChangeCurrentTaskPage(int taskId)
        {
            var taskPage = CreateTaskPage(taskId);

            StimTracker?.SendStimCode(Settings.TaskStartStimTrackerLabel);
            _timer.RestartTaskTimer();
            ProtocolWriter?.AppendTaskInfo(taskPage.Task);

            taskPage.OptionSelected +=
                (_, option) =>
                {
                    StimTracker?.SendStimCode(taskPage.Task.OptionIsCorrect(option)
                        ? Settings.AnswerRightStimTrackerLabel
                        : Settings.AnswerWrongStimTrackerLabel);
                    ProtocolWriter?.AppendRoundOptionSelect(option, _timer.GetTryTimerElapsed());
                    if (CsvProtocolWriter != null)
                    {
                        // Only checked compatibility for SplitOptionsTask
                        var task = taskPage.Task;
                        CsvProtocolWriter.WriteResponseRow(
                            task.CurrentRoundNumber,
                            task.RoundPhrase,
                            task.RightOption,
                            option,
                            _timer.GetTryTimerElapsed(),
                            _timer.GetRoundTimerElapsed(),
                            _timer.GetTaskTimerElapsed());
                    }

                    _timer.RestartTryTimer();
                };

            taskPage.RoundAttempt += RoundAttempt;
            taskPage.RoundSkip += RoundSkip;

            taskPage.NextRound += (_, _) =>
            {
                RoundEnd();
                RoundStart(taskPage.Task);
            };
            taskPage.TaskFinished += (_, _) =>
            {
                RoundEnd();

                StimTracker?.SendStimCode(Settings.TaskEndStimTrackerLabel);
                ProtocolWriter?.AppendTaskEnd(_timer.GetTaskTimerElapsed());

                if (taskId is 7 or 10)
                {
                    ProtocolWriter?.AppendExperimentEnd();
                    TasksFinished?.Invoke(this, EventArgs.Empty);
                }
                else
                    ChangeCurrentTaskPage(taskId + 1);
            };
            RoundStart(taskPage.Task);
            TaskDescriptionLabel.Content = taskPage.Task.Description;
            TaskFrame.Navigate(taskPage);
        }

        private void UpdateTaskNameLabel(BaseTask task)
        {
            TaskNameLabel.Content = $"{task.Name} {task.CurrentRoundNumber}/{task.TotalRoundsNumber}";
        }
    }
}