﻿namespace Frontend.Pages.Tasks.TaskPages
{
    public enum TranslateTaskPageLanguage
    {
        GermanToRussian,
        RussianToGerman
    }
}