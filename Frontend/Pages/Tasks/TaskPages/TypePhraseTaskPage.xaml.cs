﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Backend.Tasks.CollectPhrase;
using Frontend.Pages.Tasks.TaskPages.Controls;

namespace Frontend.Pages.Tasks.TaskPages
{
    public partial class TypeWordTaskPage
    {
        private static readonly string[] SpecialLetters = {"ä", "ö", "ü", "ẞ"};

        private string _typedWord;
        private int _replaysLeft;
        private const int ReplaysPerRound = 3;

        public TypeWordTaskPage() : base(new TypeWordTask())
        {
            InitializeComponent();

            WordTextBox.TextChanged += (sender, args) =>
            {
                if (WordTextBox.Text != "")
                    OptionSelect(WordTextBox.Text.ToLower());
            };

            foreach (var specialLetter in SpecialLetters)
            {
                var control = new RectangleAnswerOptionControl {Content = specialLetter};
                control.MouseDown += (s, args) =>
                    WordTextBox.Text += specialLetter;
                SpecialLettersPanel.Children.Add(control);
            }
        }

        protected override void OptionSelect(string option)
        {
            base.OptionSelect(option);

            FocusText();
            if (!Task.RightOption.ToLower().StartsWith(option))
            {
                WordTextBox.Text = _typedWord;
                return;
            }

            _typedWord = WordTextBox.Text;

            if (!string.Equals(Task.RightOption, _typedWord, StringComparison.CurrentCultureIgnoreCase)) return;
            WordTextBox.Foreground = Brushes.SeaGreen;
            WordTextBoxFocusContainer.Focus();
            WordTextBox.IsEnabled = false;
            new Task(() =>
            {
                Thread.Sleep(500);
                GoNextRound();
            }).Start();
        }

        private void SetReplaysLeft(int replaysLeft)
        {
            _replaysLeft = replaysLeft;
            ReplayButton.Content = $"Повторить ({replaysLeft}/{ReplaysPerRound})";
        }

        protected override void SetRoundValues()
        {
            SetReplaysLeft(ReplaysPerRound);
            _typedWord = "";
            WordTextBox.Text = "";
            WordTextBox.Foreground = Brushes.Black;
            WordTextBox.IsEnabled = true;
            FocusText();
            PlayRoundSound();
        }

        private void FocusText()
        {
            WordTextBox.Focus();
            WordTextBox.CaretIndex = int.MaxValue;
        }

        private void ReplayButton_Click(object sender, RoutedEventArgs routedEventArgs)
        {
            if (_replaysLeft == 0)
            {
                OnRoundSkip();
                GoNextRound();
                return;
            }

            PlayRoundSound();
        }

        private void PlayRoundSound()
        {
            ReplayButton.IsEnabled = false;
            FocusText();
            new Task(() =>
            {
                PlayRoundSound(true);
                Dispatcher.Invoke(() =>
                {
                    ReplayButton.IsEnabled = true;
                    SetReplaysLeft(_replaysLeft - 1);
                    OnRoundAttempt(ReplaysPerRound - _replaysLeft, ReplaysPerRound);
                    if (_replaysLeft == 0)
                        ReplayButton.Content = "Пропустить";
                });
            }).Start();
        }
    }
}