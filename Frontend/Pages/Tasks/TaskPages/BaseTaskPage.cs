﻿using System;
using System.Threading;
using System.Windows.Controls;
using Backend.Tasks;

namespace Frontend.Pages.Tasks.TaskPages
{
    public abstract class BaseTaskPage : Page
    {
        private readonly SoundsPlayer _soundsPlayer = new();

        protected BaseTaskPage(BaseTask task)
        {
            Task = task;
            Initialized += (_, _) =>
            {
                AdditionalInit();
                if (Task.IsSounded)
                    LoadRoundSound();
                SetRoundValues();
            };
        }

        public BaseTask Task { get; }
        public event EventHandler NextRound;
        public event Action<int, int> RoundAttempt;
        public event Action RoundSkip;

        public event EventHandler<string> OptionSelected;
        public event EventHandler TaskFinished;

        protected abstract void SetRoundValues();

        protected virtual void AdditionalInit()
        {
        }

        protected void GoNextRound()
        {
            new Thread(() =>
            {
                Dispatcher.Invoke(() =>
                {
                    Task.NextRound();
                    if (Task.IsComplete)
                    {
                        TaskFinished?.Invoke(this, EventArgs.Empty);
                        return;
                    }

                    if (Task.IsSounded)
                        LoadRoundSound();
                    SetRoundValues();
                    NextRound?.Invoke(this, EventArgs.Empty);
                });
            }).Start();
        }

        protected void OnRoundAttempt(int attemptNumber, int totalAttempts)
        {
            RoundAttempt?.Invoke(attemptNumber, totalAttempts);
        }

        protected void OnRoundSkip()
        {
            RoundSkip?.Invoke();
        }

        protected virtual void OptionSelect(string option)
        {
            OptionSelected?.Invoke(this, option);
        }

        private void LoadRoundSound()
        {
            _soundsPlayer.LoadSound(
                $"Sounds/{Task.Id}_{Task.CurrentRoundIndex + 1}.mp3");
        }

        protected void PlayRoundSound(bool reloadAfter = false)
        {
            _soundsPlayer.PlaySound();
            if (reloadAfter)
                LoadRoundSound();
        }
    }
}