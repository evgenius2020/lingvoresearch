﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace Frontend.Pages.Tasks.TaskPages.Controls
{
    public partial class PuzzleAnswerOptionControl
    {
        private Point _positionInBlock;

        public PuzzleAnswerOptionControl()
        {
            InitializeComponent();
            SetColor(AnswerOptionColor.Black);
        }

        public bool Movable { get; set; } = true;

        public string Title
        {
            get => (string) Label.Content;
            set => Label.Content = value;
        }

        public void SetColor(AnswerOptionColor color)
        {
            switch (color)
            {
                case AnswerOptionColor.Red:
                    Label.BorderBrush = new SolidColorBrush(Color.FromRgb(119, 59, 57));
                    Label.Background = new SolidColorBrush(Color.FromRgb(225, 124, 116));
                    break;
                case AnswerOptionColor.Black:
                    Label.BorderBrush = Brushes.Black;
                    Label.Background = Brushes.White;
                    break;
                case AnswerOptionColor.Green:
                    Label.BorderBrush = Brushes.Green;
                    Label.Background = new SolidColorBrush(Color.FromRgb(147, 200, 118));
                    break;
                case AnswerOptionColor.Gray:
                    Label.BorderBrush = Brushes.Gray;
                    Label.Background = Brushes.DimGray;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(color), color, null);
            }
        }

        private void UserControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (!Movable) return;
            _positionInBlock = e.GetPosition(VisualTreeHelper.GetParent(this) as UIElement);
            CaptureMouse();
        }

        private void UserControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (!IsMouseCaptured) return;

            var mousePosition = e.GetPosition(VisualTreeHelper.GetParent(this) as UIElement);
            RenderTransform = new TranslateTransform(
                mousePosition.X - _positionInBlock.X,
                mousePosition.Y - _positionInBlock.Y);
        }

        private void UserControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            RenderTransform = null;
            ReleaseMouseCapture();
        }
    }
}