﻿using System;
using System.Windows.Media;

namespace Frontend.Pages.Tasks.TaskPages.Controls
{
    public partial class RectangleAnswerOptionControl
    {
        public RectangleAnswerOptionControl()
        {
            InitializeComponent();
        }

        public new Brush Background
        {
            set => Border.Background = value;
            get => Border.Background;
        }

        public new object Content
        {
            get => Label.Content;
            set
            {
                Label.FontSize = Math.Max(15, Label.FontSize- value.ToString().Length * 2);
                Label.Content = value;
            }
        }
    }
}