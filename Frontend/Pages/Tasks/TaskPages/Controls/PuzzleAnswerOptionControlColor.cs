﻿namespace Frontend.Pages.Tasks.TaskPages.Controls
{
    public enum AnswerOptionColor
    {
        Black,
        Red,
        Green,
        Gray
    }
}