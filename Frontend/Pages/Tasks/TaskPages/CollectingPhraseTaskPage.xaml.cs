﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Backend.Tasks;
using Backend.Tasks.CollectPhrase;
using Backend.Tasks.CollectSentence;
using Frontend.Pages.Tasks.TaskPages.Controls;

namespace Frontend.Pages.Tasks.TaskPages
{
    public partial class CollectingTaskPage
    {
        private string currentAnswer = "";

        public CollectingTaskPage(bool withTranslate = false) : base(!withTranslate
            ? (BaseTask) new CollectWordTask()
            : new CollectSentenceTask())
        {
            InitializeComponent();

            TranslateLabel.Visibility = withTranslate ? Visibility.Visible : Visibility.Collapsed;
        }

        private void OptionSelect(string option, RectangleAnswerOptionControl control)
        {
            OptionSelect(option);

            if (Task.RightOption.StartsWith(option))
            {
                currentAnswer = option + (Task is CollectSentenceTask ? " " : "");
                control.Visibility = Visibility.Hidden;
                if (Task.OptionIsCorrect(option))
                {
                    PhraseLabel.Content = Task.RoundPhrase;
                    PhraseLabel.Foreground = Brushes.SeaGreen;
                    new Task(() =>
                    {
                        PlayRoundSound();
                        GoNextRound();
                    }).Start();
                }
                else
                {
                    PhraseLabel.Content = option + "_";
                    foreach (RectangleAnswerOptionControl c in LettersStackPanel.Children)
                        c.Background = Brushes.White;
                }
            }
            else
            {
                control.Background = Brushes.PaleVioletRed;
            }
        }

        protected override void SetRoundValues()
        {
            if (Task is CollectSentenceTask task)
            {
                TranslateLabel.FontSize = Math.Max(30, 70 - task.RoundSentenceTranslation.Length);
                TranslateLabel.Content = task.RoundSentenceTranslation;
            }

            PhraseLabel.FontSize = Math.Max(30, 70 - Task.RightOption.Length);
            PhraseLabel.Content = "_";
            PhraseLabel.Foreground = Brushes.Black;
            LettersStackPanel.Children.Clear();
            currentAnswer = "";
            foreach (var characterControl in
                Task.RoundOptions.Select(character => new RectangleAnswerOptionControl {Content = character}))
            {
                characterControl.MouseLeftButtonDown += (sender, args) =>
                    OptionSelect(currentAnswer + characterControl.Content, characterControl);
                LettersStackPanel.Children.Add(characterControl);
            }
        }
    }
}