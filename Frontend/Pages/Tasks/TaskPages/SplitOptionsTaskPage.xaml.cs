﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Backend;
using Backend.Tasks.SplitOptionsTask;

namespace Frontend.Pages.Tasks.TaskPages
{
    public partial class SplitOptionsTaskPage
    {
        private List<string>.Enumerator _roundOptions;
        private ItemsControl[] _possibleOptionsItemControls;
        private readonly StimTracker _stimTracker;
        private bool _answeringStarted;

        public SplitOptionsTaskPage(StimTracker splitOptionsStimTracker) : base(new SplitOptionsTask())
        {
            _stimTracker = splitOptionsStimTracker;
            InitializeComponent();
        }

        private static bool MouseIsNearOfOptionsItemsControl(ItemsControl optionsItemsControl)
        {
            var pos = Mouse.GetPosition(optionsItemsControl);
            return -10 <= pos.X && pos.X <= optionsItemsControl.ActualWidth + 10 &&
                   -10 <= pos.Y && pos.Y <= optionsItemsControl.ActualHeight + 10;
        }

        protected override void AdditionalInit()
        {
            base.AdditionalInit();
            _possibleOptionsItemControls = new[]
            {
                NotSureRightItemsControl, DefinitelyRightItemsControl,
                DefinitelyWrongItemsControl, NotSureWrongItemsControl
            };

            OptionLabel.MouseUp += (_, _) =>
            {
                ItemsControl selectedOption = null;
                foreach (var optionsItemsControl in _possibleOptionsItemControls)
                {
                    if (MouseIsNearOfOptionsItemsControl(optionsItemsControl))
                        selectedOption = optionsItemsControl;
                }

                if (selectedOption == null) return;
                selectedOption.Items.Add(OptionLabel.Content);

                if (_stimTracker != null)
                {
                    if (selectedOption.Tag == NotSureRightItemsControl.Tag)
                        _stimTracker.SendStimCode(Settings.SplitOptionsNotSureRightSelectedStimTrackerLabel);
                    if (selectedOption.Tag == DefinitelyRightItemsControl.Tag)
                        _stimTracker.SendStimCode(Settings.SplitOptionsDefinitelyRightSelectedStimTrackerLabel);
                    if (selectedOption.Tag == NotSureWrongItemsControl.Tag)
                        _stimTracker.SendStimCode(Settings.SplitOptionsNotSureWrongSelectedStimTrackerLabel);
                    if (selectedOption.Tag == DefinitelyWrongItemsControl.Tag)
                        _stimTracker.SendStimCode(Settings.SplitOptionsDefinitelyWrongSelectedStimTrackerLabel);
                }

                // Answer is {option},{answer}
                OptionSelect($"\"{OptionLabel.Content}\",\"{selectedOption.Tag}\"");
            };

            OptionLabel.MouseMove += (_, _) =>
            {
                foreach (var optionsItemsControl in _possibleOptionsItemControls)
                {
                    optionsItemsControl.BorderBrush =
                        MouseIsNearOfOptionsItemsControl(optionsItemsControl) ? Brushes.Black : null;
                }
            };

            OptionLabel.MouseDown += (_, _) =>
            {
                if (_answeringStarted) return;
                _answeringStarted = true;
                _stimTracker?.SendStimCode(Settings.SplitOptionsAnsweringStartedStimTrackerLabel);
            };
        }

        protected override void SetRoundValues()
        {
            var task = Task as SplitOptionsTask;
            _roundOptions = task.RoundOptions.GetEnumerator();
            _roundOptions.MoveNext();
            PhraseLabel.Content = task.RoundPhrase;
            NotesLabel.Content = task.CurrentRoundNotes;
            OptionLabel.Content = _roundOptions.Current;
            _stimTracker?.SendStimCode((string)OptionLabel.Content == task.RightOption
                ? Settings.SplitOptionsRightOptionShownStimTrackerLabel
                : Settings.SplitOptionsWrongOptionShownStimTrackerLabel);
            foreach (var optionsItemsControl in _possibleOptionsItemControls)
                optionsItemsControl?.Items.Clear();
        }

        protected override void OptionSelect(string option)
        {
            base.OptionSelect(option);
            _answeringStarted = false;
            foreach (var optionsItemsControl in _possibleOptionsItemControls)
                optionsItemsControl.BorderBrush = null;
            if (_roundOptions.MoveNext())
                OptionLabel.Content = _roundOptions.Current;
            else
                GoNextRound();
        }
    }
}