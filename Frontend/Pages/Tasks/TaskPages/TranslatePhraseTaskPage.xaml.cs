﻿using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using Backend.Tasks;
using Backend.Tasks.SelectRightAnswer;

namespace Frontend.Pages.Tasks.TaskPages
{
    public partial class TranslateTaskPage
    {
        private readonly Label[] _optionLabels = new Label[4];

        public TranslateTaskPage(TranslateTaskPageLanguage language) : base(
            language == TranslateTaskPageLanguage.RussianToGerman
                ? (BaseTask) new TranslateRussianToGermanTask()
                : new TranslateGermanToRussianTask())
        {
            InitializeComponent();
        }

        private void OptionSelect(string option, Label optionLabel)
        {
            OptionSelect(option);

            if (!Task.OptionIsCorrect(option))
            {
                optionLabel.Background = Brushes.PaleVioletRed;
            }
            else
            {
                optionLabel.Background = Brushes.PaleGreen;
                foreach (Label label in OptionsGrid.Children)
                    label.IsEnabled = false;

                new Task(() =>
                {
                    PlayRoundSound();
                    GoNextRound();
                }).Start();
            }
        }

        protected override void AdditionalInit()
        {
            for (var i = 0; i < 4; i++)
            {
                var optionLabel = new Label();
                _optionLabels[i] = optionLabel;
                optionLabel.MouseEnter += (sender, args) =>
                {
                    if (optionLabel.Background == Brushes.White)
                        optionLabel.Background = Brushes.LightGray;
                };
                optionLabel.MouseLeave += (sender, args) =>
                {
                    if (optionLabel.Background == Brushes.LightGray)
                        optionLabel.Background = Brushes.White;
                };
                optionLabel.MouseLeftButtonDown += (sender, args) =>
                    OptionSelect((string) optionLabel.Content, optionLabel);

                OptionsGrid.Children.Add(optionLabel);
            }
        }

        protected override void SetRoundValues()
        {
            PhraseLabel.Content = Task.RoundPhrase;
            using (var optionsEnumerator = Task.RoundOptions.GetEnumerator())
            {
                foreach (var optionLabel in _optionLabels)
                {
                    optionsEnumerator.MoveNext();

                    optionLabel.IsEnabled = true;
                    optionLabel.Background = Brushes.White;
                    optionLabel.Content = optionsEnumerator.Current;
                }
            }
        }
    }
}