﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Backend.Tasks;
using Backend.Tasks.SelectRightAnswer;
using Frontend.Pages.Tasks.TaskPages.Controls;

namespace Frontend.Pages.Tasks.TaskPages
{
    public partial class SelectRightAnswerTaskPage
    {
        private readonly PuzzleAnswerOptionControl[] _answerOptions = new PuzzleAnswerOptionControl[4];
        private PuzzleAnswerOptionControl _selectedOptionControl;

        public SelectRightAnswerTaskPage(bool withTranslate = false) : base(
            withTranslate ? (BaseTask) new SelectRightAnswerWithTranslateTask() : new SelectRightAnswerTask())
        {
            InitializeComponent();

            TranslateLabel.Visibility = withTranslate ? Visibility.Visible : Visibility.Collapsed;
        }

        protected override void AdditionalInit()
        {
            for (var i = 0; i < 4; i++)
            {
                var answerOption = new PuzzleAnswerOptionControl();
                _answerOptions[i] = answerOption;
                answerOption.MouseMove += (sender, args) =>
                {
                    if (!answerOption.Movable) return;
                    var pos = Mouse.GetPosition(OptionPlace);
                    if (pos.X > -OptionPlace.ActualWidth / 2 && pos.X < OptionPlace.ActualWidth * 1.5 &&
                        pos.Y > -OptionPlace.ActualHeight / 2 && pos.Y < OptionPlace.ActualHeight * 1.5)
                    {
                        _selectedOptionControl = answerOption;
                        OptionPlace.SetColor(AnswerOptionColor.Gray);
                        OptionPlace.Title = answerOption.Title;
                    }
                    else
                        UnselectOption();
                };
                answerOption.MouseUp += (sender, args) =>
                {
                    if (!answerOption.Movable) return;
                    if (_selectedOptionControl != null)
                        OptionSelect(_selectedOptionControl.Title);
                };
                OptionsStackPanel.Children.Add(answerOption);
            }

            OptionPlace.Movable = false;
        }

        private void UnselectOption()
        {
            _selectedOptionControl = null;
            OptionPlace.SetColor(AnswerOptionColor.Black);
            OptionPlace.Title = "………";
        }

        protected override void OptionSelect(string option)
        {
            base.OptionSelect(option);

            if (Task.OptionIsCorrect(option))
            {
                OptionPlace.SetColor(AnswerOptionColor.Green);
                _selectedOptionControl.Visibility = Visibility.Hidden;
                foreach (PuzzleAnswerOptionControl opt in OptionsStackPanel.Children)
                    opt.Movable = false;
                OptionPlace.SetColor(AnswerOptionColor.Green);
                OptionPlace.Title = Task.RightOption;
                new Task(() =>
                {
                    PlayRoundSound();
                    GoNextRound();
                }).Start();
            }
            else
            {
                _selectedOptionControl.SetColor(AnswerOptionColor.Red);
                UnselectOption();
            }
        }

        protected override void SetRoundValues()
        {
            if (Task is SelectRightAnswerWithTranslateTask task)
                TranslateLabel.Content = task.RoundPhraseTranslation;

            OptionPlace.SetColor(AnswerOptionColor.Black);

            var roundPhrase = Task.RoundPhrase.Split('…');
            LeftPhraseLabel.Content = roundPhrase[0];
            UnselectOption();
            RightPhraseLabel.Content = roundPhrase[1];

            var enumerator = _answerOptions.GetEnumerator();
            enumerator.MoveNext();
            foreach (var roundOption in Task.RoundOptions)
            {
                var option = enumerator.Current as PuzzleAnswerOptionControl;
                option.Visibility = Visibility.Visible;
                option.SetColor(AnswerOptionColor.Black);
                option.Title = roundOption;
                option.Movable = true;
                enumerator.MoveNext();
            }
        }
    }
}