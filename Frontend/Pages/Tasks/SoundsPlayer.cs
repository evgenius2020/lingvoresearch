﻿using System.Threading;
using System.Windows;
using NAudio.Wave;

namespace Frontend.Pages.Tasks
{
    public class SoundsPlayer
    {
        private readonly WaveOutEvent _player;
        private bool _hasSound;

        public SoundsPlayer()
        {
            _player = new WaveOutEvent();
        }

        public void LoadSound(string filename)
        {
            if (_hasSound)
                _player.Dispose();
            _player.Init(new AudioFileReader(filename));
            _hasSound = true;
        }

        public void PlaySound()
        {
            if (!_hasSound)
            {
                MessageBox.Show("No audio loaded!");
                return;
            }

            _player.Play();
            while (_player.PlaybackState != PlaybackState.Stopped)
                Thread.Sleep(100);

            _player.Dispose();
            _hasSound = false;
        }
    }
}