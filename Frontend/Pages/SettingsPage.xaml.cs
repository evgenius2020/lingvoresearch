﻿using System;
using System.IO;
using System.Windows.Forms;
using Backend;

namespace Frontend.Pages
{
    public partial class SettingsPage
    {
        public SettingsPage(Settings settings)
        {
            InitializeComponent();

            var participantInfoPage = new ParticipantInfoPage();
            var stimTrackerPage = new StimTrackerPage(stimtracker =>
            {
                settings.StimTracker = stimtracker;
                settings.SplitOptionsStimTracker = stimtracker;
            });
            var protocolPage = new ProtocolPage();

            ParticipantInfoFrame.Navigate(participantInfoPage);
            StimTrackerFrame.Navigate(stimTrackerPage);
            ProtocolFrame.Navigate(protocolPage);

            StartButton.Click += (_, _) =>
            {
                settings.ParticipantName = participantInfoPage.ParticipantNameTextBox.Text;
                int.TryParse(participantInfoPage.ParticipantAgeTextBox.Text, out var age);
                settings.ParticipantAge = age;
                try
                {
                    var stream = new FileStream(protocolPage.FileNameTextBox.Text, FileMode.Append);
                    var splitTaskMode = (bool)StartNewTaskCheckBox.IsChecked;
                    if (splitTaskMode)
                    {
                        settings.CsvProtocolWriter = new CsvProtocolWriter(new StreamWriter(stream));
                        settings.StimTracker = null;
                    }
                    else
                    {
                        settings.ProtocolWriter = new ProtocolWriter(new StreamWriter(stream));
                        settings.SplitOptionsStimTracker = null;
                    }

                    StartPressed?.Invoke(this, EventArgs.Empty);
                }
                catch
                {
                    MessageBox.Show(
                        "Не удалось создать файл протокола. " +
                        "Убедитесь, что программа имеет доступ в эту дирректорию.");
                }
            };
        }

        public event EventHandler StartPressed;
    }
}