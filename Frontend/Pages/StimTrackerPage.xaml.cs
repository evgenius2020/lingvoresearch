﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using Backend;

namespace Frontend.Pages
{
    public partial class StimTrackerPage
    {
        private readonly Action<StimTracker> _connectionChanged;
        private StimTracker _stimTrackerConnection;
        private readonly List<string> _comPortNames;

        public StimTrackerPage(Action<StimTracker> connectionChanged)
        {
            InitializeComponent();

            _connectionChanged = connectionChanged;

            _comPortNames = StimTracker.GetPortNames.ToList();
            foreach (var portName in _comPortNames)
            {
                var comPortNamesItemsIndex = ComPortNames.Items.Add(portName);
                var conn = new StimTracker(portName);
                Thread.Sleep(500);
                var connectionStatus = conn.GetConnectionStatus();
                conn.CloseConnection();
                if (connectionStatus.Contains("StimTracker"))
                    connectionStatus = "StimTracker";
                ComPortNames.Items[comPortNamesItemsIndex] =
                    $"{portName} [{connectionStatus}]";
                
                if (connectionStatus == "StimTracker")
                    ComPortNames.SelectedIndex = comPortNamesItemsIndex;
            }

            if (ComPortNames.SelectedIndex == -1)
                ComPortNames.SelectedIndex = 0;

            StimCodeSlider.Minimum = 1;
            StimCodeSlider.Maximum = 14;
        }

        private void TestButton_Clicked(object sender, RoutedEventArgs e)
        {
            var code = (int) StimCodeSlider.Value;
            if (_stimTrackerConnection != null && _stimTrackerConnection.SendStimCode(code))
                MessageBox.Show($"Был отправлен сигнал {code}");
            else
                MessageBox.Show("Не удалось отправить сигнал");
        }

        private void ComPortNames_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedIndex = (sender as ComboBox)?.SelectedIndex;
            if (selectedIndex == null)
                return;
            _stimTrackerConnection?.CloseConnection();
            _stimTrackerConnection = new StimTracker(_comPortNames[(int) selectedIndex]);
            _connectionChanged?.Invoke(_stimTrackerConnection);
        }

        private void StimCodeSlider_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            TestButton.Content = $"Test({e.NewValue})";
        }
    }
}