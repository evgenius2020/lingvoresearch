﻿using System;

namespace Frontend.Pages
{
    public partial class FinishPage
    {
        public FinishPage()
        {
            InitializeComponent();

            FinishButton.Click += (sender, args) => FinishPressed?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler FinishPressed;
    }
}