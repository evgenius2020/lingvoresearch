﻿using System.Windows.Input;

namespace Frontend.Pages
{
    public partial class ParticipantInfoPage
    {
        public ParticipantInfoPage()
        {
            InitializeComponent();
            
            ParticipantNameTextBox.Text = Settings.DefaultParticipantName;
            ParticipantAgeTextBox.Text = Settings.DefaultParticipantAge.ToString();
        }

        private void AgeValidator(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !(
                int.TryParse(ParticipantAgeTextBox.Text + e.Text, out var age) &&
                age > 0 &&
                age <= 100);
        }
    }
}