﻿using System.Windows;
using System.Windows.Forms;

namespace Frontend.Pages
{
    public partial class ProtocolPage
    {
        public ProtocolPage()
        {
            InitializeComponent();

            FileNameTextBox.Text = Settings.DefaultProtocolWriterPath + $@"\{Settings.DefaultProtocolFilename}";
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var fileDialog = new SaveFileDialog
            {
                Filter = "|*.txt",
                InitialDirectory = Settings.DefaultProtocolWriterPath,
                FileName = Settings.DefaultProtocolFilename
            };
            if (fileDialog.ShowDialog() == DialogResult.OK)
                FileNameTextBox.Text = fileDialog.FileName;
        }
    }
}