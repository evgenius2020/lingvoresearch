﻿using System;
using Backend;

namespace Frontend
{
    public class Settings
    {
        public const int ExperimentStartStimTrackerLabel = 1;
        public const int ExperimentEndStimTrackerLabel = 2;
        public const int TaskStartStimTrackerLabel = 3;
        public const int TaskEndStimTrackerLabel = 4;
        public const int RoundStartStimTrackerLabel = 5;
        public const int RoundEndStimTrackerLabel = 6;
        public const int AnswerRightStimTrackerLabel = 7;
        public const int AnswerWrongStimTrackerLabel = 8;
        public const int RoundAttemptStimTrackerLabel = 9;

        public const int SplitOptionsExperimentStartStimTrackerLabel = 1;
        public const int SplitOptionsExperimentEndStimTrackerLabel = 2;
        public const int SplitOptionsRoundStartStimTrackerLabel = 3;
        public const int SplitOptionsRoundEndStimTrackerLabel = 4;
        public const int SplitOptionsRightOptionShownStimTrackerLabel = 5;
        public const int SplitOptionsWrongOptionShownStimTrackerLabel = 6;
        public const int SplitOptionsAnsweringStartedStimTrackerLabel = 7;
        public const int SplitOptionsDefinitelyRightSelectedStimTrackerLabel = 8;
        public const int SplitOptionsNotSureRightSelectedStimTrackerLabel = 9;
        public const int SplitOptionsDefinitelyWrongSelectedStimTrackerLabel = 10;
        public const int SplitOptionsNotSureWrongSelectedStimTrackerLabel = 11;

        public const string Version = "2.2";
        public static readonly DateTime ExperimentStartTime = DateTime.Now;

        public const string DefaultParticipantName = "Участник";
        public const int DefaultParticipantAge = 18;

        public static readonly string DefaultProtocolWriterPath =
            Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

        public static readonly string DefaultProtocolFilename = $"{ExperimentStartTime:dd-M-yyyy HH-mm}.txt";

        public string ParticipantName { get; set; }
        public int ParticipantAge { get; set; }
        public StimTracker StimTracker { get; set; }
        public StimTracker SplitOptionsStimTracker { get; set; }
        public ProtocolWriter ProtocolWriter { get; set; }
        public CsvProtocolWriter CsvProtocolWriter { get; set; }
    }
}