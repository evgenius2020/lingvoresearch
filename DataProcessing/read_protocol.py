import pandas as pd

protocol_filename = '10-10-2021 18-35.txt'

# First 2 rows of protocol is experiment information.
experiment_info = pd.read_csv(protocol_filename,
                              error_bad_lines=False,
                              nrows=2)

protocol = pd.read_csv(protocol_filename,
                       skiprows=2,
                       header=0)

rounds_unified = pd.read_csv('unified.csv')
rounds_unified['join_key'] = rounds_unified['russian_phrase'] + rounds_unified['option']
rounds_unified = rounds_unified.set_index('join_key')
rounds_unified = rounds_unified.drop(columns=['russian_phrase', 'option'])

protocol['join_key'] = protocol['russian_phrase'] + protocol['option']
protocol = protocol.join(rounds_unified, on='join_key')
protocol = protocol.drop(columns=['join_key'])

protocol.to_csv('protocol_ready.csv', index=False)
