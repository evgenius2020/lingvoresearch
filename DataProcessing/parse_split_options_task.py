import pandas as pd

# p.s.: To understand this script, open source .xsls file first.

sheets = pd.read_excel('./answers_10_10_2021.xlsx',
                       sheet_name=None,
                       index_col=[0, 1, 2, 3],
                       header=None)
# Source .xslx has 5 sheets of phrases, so merge them.
df = None
for sheet_name, sheet_content in list(sheets.items())[:5]:
    # Set phrase_semantic_group column at left.
    sheet_content.insert(
        loc=0, column='phrase_semantic_group', value=sheet_name)
    if df is None:
        df = sheet_content
    else:
        df = pd.concat([df, sheet_content])

# Source .xslx is multi index, so open it properly. Index names will be at left, so reorder them.
df = df.reset_index(drop=False)
df.columns = ['phrase_type', 'phrase_number', 'notes', 'russian_phrase', 'phrase_semantic_group'] + \
             [f'unparsed#{i}' for i in range(len(df.columns) - 5)]
df = df[['phrase_semantic_group', 'phrase_number',
         'phrase_type', 'russian_phrase', 'notes'] + list(df.columns[5:])]

# Make unified phrase options table.
df_unified = pd.DataFrame(columns=['phrase_semantic_group', 'phrase_number', 'phrase_type', 'russian_phrase', 'notes',
                                   'option', 'option_type'])
for phrase_semantic_group in df['phrase_semantic_group'].unique():
    phrases_for_semantic_group = df[df['phrase_semantic_group']
                                    == phrase_semantic_group]
    for phrase_type in phrases_for_semantic_group['phrase_type'].unique():
        phrases_for_phrase_type = df[df['phrase_type'] == phrase_type].copy()

        # First five columns has right structure, others needs to be unified.
        all_answers_for_phrase_type = pd.DataFrame(columns=df_unified.columns)
        for answer_type_col_index in range(5, len(phrases_for_phrase_type.columns)):
            answers_for_answer_type = phrases_for_phrase_type.iloc[:, list(
                range(5)) + [answer_type_col_index]].copy()
            answers_for_answer_type['option_type'] = answers_for_answer_type.iloc[0, 5]
            answers_for_answer_type.columns = df_unified.columns
            # Header row.
            answers_for_answer_type = answers_for_answer_type.iloc[1:, :]
            all_answers_for_phrase_type = pd.concat(
                [all_answers_for_phrase_type, answers_for_answer_type])
            all_answers_for_phrase_type = all_answers_for_phrase_type.dropna()
        df_unified = pd.concat([df_unified, all_answers_for_phrase_type])

df_unified['russian_phrase'] = df_unified['russian_phrase'].str.strip()
df_unified['option'] = df_unified['option'].str.strip()
df_unified['notes'] = df_unified['notes'].str.replace('[ \t\n]{1,}', ' ', regex=True)
df_unified = df_unified[df_unified['option'] != '']
df_unified['option_type'] = df_unified['option_type'].str.strip().str.lower()
df_unified = df_unified.drop_duplicates()
df_unified = df_unified[~(
        df_unified['option'].str.lower() == df_unified['option_type'])]
df_unified = df_unified.sort_values(
    by=['phrase_semantic_group', 'phrase_number', 'option'])


def merge_options(phrase_df):
    options = '\n'.join(phrase_df['option'].values)
    right_option = phrase_df[phrase_df['option_type']
                             == 'правильный ответ']['option'].values[0]
    print(f"""new SplitOptionsTaskRoundInfo(
        "{phrase_df['russian_phrase'].values[0]}",\n
        "{phrase_df['notes'].values[0]}",\n 
        new[] {{"{'", "'.join(phrase_df['option'].values)}"}},\n
        "{right_option}"),""")
    return pd.DataFrame(data=[[options, right_option]],
                        columns=['options', 'right_option']).iloc[0]


final_df = df_unified.groupby(
    ['phrase_semantic_group', 'phrase_number', 'phrase_type', 'russian_phrase', 'notes']).apply(merge_options)

df_unified = df_unified.set_index(
    ['phrase_semantic_group', 'phrase_number', 'phrase_type', 'russian_phrase', 'notes'])
df_unified.to_csv('unified.csv')
with pd.ExcelWriter('unified.xlsx') as writer:
    df_unified.to_excel(writer)

final_df.to_csv('final.csv')
with pd.ExcelWriter('final.xlsx') as writer:
    final_df.to_excel(writer)
