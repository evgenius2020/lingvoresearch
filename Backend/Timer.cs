﻿using System.Diagnostics;

namespace Backend
{
    public class Timer
    {
        private readonly Stopwatch _taskTimer;
        private readonly Stopwatch _roundTimer;
        private readonly Stopwatch _tryTimer;

        public Timer()
        {
            _taskTimer = new Stopwatch();
            _roundTimer = new Stopwatch();
            _tryTimer = new Stopwatch();
        }
        
        public void RestartTaskTimer()
        {
            _taskTimer.Restart();
            _roundTimer.Restart();
            _tryTimer.Restart();;
        }

        public void RestartRoundTimer()
        {
            _roundTimer.Restart();
            _tryTimer.Restart();
        }

        public void RestartTryTimer() => _tryTimer.Restart();

        public int GetTaskTimerElapsed() => (int) _taskTimer.Elapsed.TotalMilliseconds;
        public int GetRoundTimerElapsed() => (int) _roundTimer.Elapsed.TotalMilliseconds;
        public int GetTryTimerElapsed() => (int) _tryTimer.Elapsed.TotalMilliseconds;
    }
}