﻿using System;
using System.IO;
using System.Linq;
using Backend.Tasks;

namespace Backend
{
    public class ProtocolWriter
    {
        private readonly StreamWriter _writer;

        public ProtocolWriter(StreamWriter writer)
        {
            _writer = writer;
        }

        private void AppendString(string str)
        {
            _writer.WriteLine(str);
            _writer.Flush();
        }

        public void AppendParticipantInfo(string name, int age) =>
            AppendString($"Имя: {name}\nВозраст: {age}");

        public void AppendExperimentStart(DateTime now, string version)
        {
            AppendString($"\n===================");
            AppendString("Начало эксперимента");
            AppendString($"LingvoResearch v{version}");
            AppendString(now.ToString());
        }
        
        public void AppendExperimentEnd() =>
            AppendString("Конец эксперимента");

        public void AppendTaskInfo(BaseTask task)
        {
            AppendString(
                $"\nЗадание #{task.Id}\n" +
                $"Название: {task.Name}\n" +
                $"Описание: {task.Description}");
        }

        public void AppendTaskEnd(int delay) =>
            AppendString($"Задание завершено за {delay} мс.");

        public void AppendRoundInfo(BaseTask task) =>
            AppendString(
                $"\n= Раунд #{task.CurrentRoundNumber}\n" +
                $"= Фраза: {task.RoundPhrase}\n" +
                $"= Варианты: [{string.Join(", ", task.RoundOptions.Select(s => $"'{s}'"))}]\n" +
                $"= Правильный ответ: {task.RightOption}");

        public void AppendRoundEnd(int delay) =>
            AppendString($"= Раунд завершен за {delay} мс.");

        public void AppendRoundOptionSelect(string option, int delay) =>
            AppendString($"=== Выбран вариант '{option}' за {delay} мс.");

        public void AppendRoundAttempt(int attemptNumber, int totalAttempts, int delay) =>
            AppendString($"=== Попытка {attemptNumber}/{totalAttempts} на {delay}мс.");

        public void AppendRoundSkip() => AppendString("= Раунд пропущен.");
    }
}