﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SerialPortLib2;
using SerialPortLib2.Port;

namespace Backend
{
    public class StimTracker
    {
        // Hardcoded magic.
        private static readonly byte[][] ConvertedStimCodes = new[]
        {
            "6d6800006d7032000000", "6d6840006d7032000000", "6d6820006d7032000000", "6d6860006d7032000000",
            "6d6810006d7032000000", "6d6850006d7032000000", "6d6830006d7032000000", "6d6870006d7032000000",
            "6d6808006d7032000000", "6d6848006d7032000000", "6d6828006d7032000000", "6d6868006d7032000000",
            "6d6818006d7032000000", "6d6858006d7032000000", "6d6838006d7032000000"
        }.Select(ToByteArray).ToArray();

        private readonly SerialPortInput _connection;
        private string _connectionStatus;

        public StimTracker(string portName)
        {
            _connection = new SerialPortInput(portName, 115200,
                Parity.None, 8, StopBits.One, Handshake.None, false);
            _connection.Connect();
            
            _connectionStatus = "...";
            _connection.MessageReceived += (sender, args) => 
                _connectionStatus = Encoding.ASCII.GetString(args.Data);
            if (_connection.IsConnected && _connection.SendMessage(ToByteArray("5f643100"))) return;
            _connectionStatus = "Connection failed";
        }

        public string GetConnectionStatus()
        {
            return _connectionStatus;
        }
        
        public static IEnumerable<string> GetPortNames => SerialPortInput.GetPorts();

        private static byte[] ToByteArray(string hexString)
        {
            var res = new byte[hexString.Length / 2];
            for (var i = 0; i < hexString.Length; i += 2)
                res[i / 2] = Convert.ToByte(hexString.Substring(i, 2), 16);
            return res;
        }

        public bool SendStimCode(int stimCode)
        {
            return _connection != null &&
                   1 <= stimCode && stimCode <= 14 &&
                   _connection.SendMessage(ConvertedStimCodes[stimCode]);
        }

        public void CloseConnection()
        {
            _connection?.Disconnect();
        }
    }
}