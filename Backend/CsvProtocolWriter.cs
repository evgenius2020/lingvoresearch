﻿using System;
using System.IO;

namespace Backend
{
    public class CsvProtocolWriter
    {
        private readonly TextWriter _textWriter;

        public CsvProtocolWriter(TextWriter textWriter)
        {
            _textWriter = textWriter;
        }

        public void WriteExperimentInfo(string programVersion, DateTime date, string subjectName, int subjectAge)
        {
            _textWriter.WriteLine("version,date,subject,age"); // Header
            _textWriter.WriteLine($"\"{programVersion}\",\"{date}\",\"{subjectName}\",{subjectAge}");
        }

        public void WriteResponseHeader()
        {
            _textWriter.WriteLine(
                "round,russian_phrase,right_option,option,answer_type,answerRT,round_time,experiment_time");
        }

        public void WriteResponseRow(int roundNumber,
            string russianPhrase,
            string rightOption,
            string answer,
            int answerTime,
            int roundTime,
            int experimentTime)
        {
            // answer is "{option}","{answer_type}"
            _textWriter.WriteLine(
                $"{roundNumber},\"{russianPhrase}\",\"{rightOption}\",{answer},{answerTime},{roundTime},{experimentTime}");
            _textWriter.Flush();
        }
    }
}