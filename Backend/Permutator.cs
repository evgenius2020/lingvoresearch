﻿using System;
using System.Collections;

namespace Backend
{
    public static class Permutator
    {
        private static readonly Random Random = new Random();

        public static ArrayList Permute(ICollection collection)
        {
            var srcCopy = new ArrayList(collection.Count);
            srcCopy.AddRange(collection);

            var res = new ArrayList(collection.Count);

            for (var i = 0; i < collection.Count; i++)
            {
                var el = srcCopy[Random.Next(srcCopy.Count)];
                srcCopy.Remove(el);
                res.Add(el);
            }

            return res;
        }
    }
}