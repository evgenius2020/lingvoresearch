﻿namespace Backend.Tasks.CollectPhrase
{
    public class TypeWordTask : BaseTask
    {
        public static readonly BaseTaskRoundInfo[] RoundInfos =
        {
            new CollectWordTaskRoundInfo("Nachtisch"),
            new CollectWordTaskRoundInfo("Gewürze"),
            new CollectWordTaskRoundInfo("per Karte"),
            new CollectWordTaskRoundInfo("bar"),
            new CollectWordTaskRoundInfo("Speisekarte"),
            
            new CollectWordTaskRoundInfo("Stimmt so"),
            new CollectWordTaskRoundInfo("Bratkartoffeln"),
            new CollectWordTaskRoundInfo("Brathähnchen"),
            new CollectWordTaskRoundInfo("Bratwurst"),
            new CollectWordTaskRoundInfo("Wurstbrot"),
            
            new CollectWordTaskRoundInfo("Würstchen"),
            new CollectWordTaskRoundInfo("Wurstteller"),
            new CollectWordTaskRoundInfo("Käseteller"),
            new CollectWordTaskRoundInfo("Käsebrot"),
            new CollectWordTaskRoundInfo("Fischplatte"),
            
            new CollectWordTaskRoundInfo("Schwarzbrot"),
            new CollectWordTaskRoundInfo("Vollkornbrot"),
            new CollectWordTaskRoundInfo("Brötchen"),
            new CollectWordTaskRoundInfo("Obstsalat"),
            new CollectWordTaskRoundInfo("Gemüsesalat"),
            
            new CollectWordTaskRoundInfo("Getränk"),
            new CollectWordTaskRoundInfo("Vorspeise"),
            new CollectWordTaskRoundInfo("Hauptgang"),
            new CollectWordTaskRoundInfo("Kellner"),
            new CollectWordTaskRoundInfo("scharf"),
            
            new CollectWordTaskRoundInfo("satt"),
            new CollectWordTaskRoundInfo("süß"),
            new CollectWordTaskRoundInfo("fett"),
            new CollectWordTaskRoundInfo("mögen"),
            new CollectWordTaskRoundInfo("schmecken"),
            
        };

        public TypeWordTask() : base(
            5,
            "Напечатай слово",
            "Напечатай озвученное слово с помощью клавиатуры",
            RoundInfos)
        {
        }
    }
}