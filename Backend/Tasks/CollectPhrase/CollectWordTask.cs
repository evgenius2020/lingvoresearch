﻿namespace Backend.Tasks.CollectPhrase
{
    public class CollectWordTask : BaseTask
    {
        public static readonly BaseTaskRoundInfo[] RoundInfos =
        {
            new CollectWordTaskRoundInfo("Pfeffer"),
            new CollectWordTaskRoundInfo("sauer"),
            new CollectWordTaskRoundInfo("süß"),
            new CollectWordTaskRoundInfo("salzig"),
            new CollectWordTaskRoundInfo("Gewürze"),
            
            new CollectWordTaskRoundInfo("scharf"),
            new CollectWordTaskRoundInfo("Brathähnchen"),
            new CollectWordTaskRoundInfo("Gemüsesalat"),
            new CollectWordTaskRoundInfo("mögen"),
            new CollectWordTaskRoundInfo("schmecken"),
            
            new CollectWordTaskRoundInfo("Käseteller"),
            new CollectWordTaskRoundInfo("Käsebrot"),
            new CollectWordTaskRoundInfo("Vollkornbrot"),
            new CollectWordTaskRoundInfo("Wurst"),
            new CollectWordTaskRoundInfo("Würstchen"),
            
            new CollectWordTaskRoundInfo("Bratwurst"),
            new CollectWordTaskRoundInfo("Getränk"),
            new CollectWordTaskRoundInfo("Apfelsaft"),
            new CollectWordTaskRoundInfo("Kuchen"),
            new CollectWordTaskRoundInfo("Schlagsahne"),
            
            new CollectWordTaskRoundInfo("Obst"),
            new CollectWordTaskRoundInfo("Fischplatte"),
            new CollectWordTaskRoundInfo("Schweinebraten"),
            new CollectWordTaskRoundInfo("Rindfleisch"),
            new CollectWordTaskRoundInfo("Brötchen"),
            
            new CollectWordTaskRoundInfo("Beilage"),
            new CollectWordTaskRoundInfo("Gemüse"),
            new CollectWordTaskRoundInfo("Gericht"),
            new CollectWordTaskRoundInfo("Speisekarte"),
            new CollectWordTaskRoundInfo("Bratkartoffeln")
        };

        public CollectWordTask() : base(
            4,
            "Соберите слово",
            "Нажимайте для этого буквы в правильном порядке",
            RoundInfos)
        {
        }
    }
}