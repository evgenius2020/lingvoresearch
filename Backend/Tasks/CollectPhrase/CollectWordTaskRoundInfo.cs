﻿using System.Linq;

namespace Backend.Tasks.CollectPhrase
{
    public class CollectWordTaskRoundInfo : BaseTaskRoundInfo
    {
        public CollectWordTaskRoundInfo(string phrase) : base(
            phrase,
            phrase.ToLower().Select(c => c.ToString()).ToArray(),
            phrase.ToLower())
        {
        }
    }
}