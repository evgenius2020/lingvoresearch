﻿namespace Backend.Tasks.SplitOptionsTask
{
    public class SplitOptionsTaskRoundInfo : BaseTaskRoundInfo
    {
        private int OptionsAreSetCount { get; set; }

        public bool AllOptionsAreSet => OptionsAreSetCount == Options.Length;
        public string Notes { get; }

        public SplitOptionsTaskRoundInfo(string phrase, string notes, string[] options, string rightOption) :
            base(phrase, options, rightOption)
        {
            OptionsAreSetCount = 0;
            Notes = notes;
        }

        public bool CheckOption(string option)
        {
            OptionsAreSetCount += 1;
            return option == RightOption;
        }
    }
}