﻿namespace Backend.Tasks.SplitOptionsTask
{
    public class SplitOptionsTask : BaseTask
    {
        private static readonly BaseTaskRoundInfo[] RoundInfos =
        {
            new SplitOptionsTaskRoundInfo(
                "Мама всегда пьет чай.",
                "мама - die Mutter; всегда - immer; пить - trinken; чай - Tee",
                new[] { "Immer Mutter trinkt Tee.", "Mutter immer trinkt Tee.", "Mutter trinkt immer Tee." },
                "Mutter trinkt immer Tee."),
            new SplitOptionsTaskRoundInfo(
                "Травяной чай пахнет чудесно.",
                "травяной чай - der Kräutertee; пахнуть -duften; чудесно - herrlich",
                new[]
                {
                    "Der Kräutertee duftet herrlich.", "Es duftet herrlich nach Kräutertee.",
                    "Man duftet herrlich Kräutertee."
                },
                "Der Kräutertee duftet herrlich."),
            new SplitOptionsTaskRoundInfo(
                "Сиделка не придет сегодня.",
                "сиделка - die Pflegerin; приходить - kommen; сегодня - heute",
                new[]
                {
                    "Die Pflegerin kein kommt heute.", "Die Pflegerin kommt heute kein.",
                    "Die Pflegerin kommt heute nein.", "Die Pflegerin kommt heute nicht.",
                    "Die Pflegerin kommt nicht heute.", "Die Pflegerin nein kommt heute.",
                    "Die Pflegerin nicht heute kommt.", "Die Pflegerin nicht kommt heute."
                },
                "Die Pflegerin kommt heute nicht."),
            new SplitOptionsTaskRoundInfo(
                "Здесь пьют чай с маслом и солью.",
                "здесь - hier; пить - trinken; чай - der Tee; с маслом и солью - mit Butter und Salz",
                new[]
                {
                    "Hier man trinkt Tee mit Butter und Salz.", "Hier trinken Sie Tee mit Butter und Salz.",
                    "Hier trinken Tee mit Butter und Salz.", "Hier trinken sie Tee mit Butter und Salz.",
                    "Hier trinkt Tee mit Butter und Salz.", "Hier trinkt es Tee mit Butter und Salz.",
                    "Hier trinkt man Tee mit Butter und Salz."
                },
                "Hier trinkt man Tee mit Butter und Salz."),
            new SplitOptionsTaskRoundInfo(
                "Там не понимают этого.",
                "там - dort; понимать - verstehen; это - das",
                new[]
                {
                    "Dort nicht verstehen das.", "Dort nicht versteht man das.", "Dort verstehen das nicht.",
                    "Dort verstehen sie das nicht.", "Dort versteht es das nicht.", "Dort versteht man das kein.",
                    "Dort versteht man das nicht.", "Dort versteht man nicht das."
                },
                "Dort versteht man das nicht."),
            new SplitOptionsTaskRoundInfo(
                "Сейчас в кино не ходят.",
                "сейчас - heutzutage; в кино - ins Kino; ходить - gehen",
                new[]
                {
                    "Heutzutage gehen Sie ins Kino nicht.", "Heutzutage gehen ins Kino nein.",
                    "Heutzutage gehen ins Kino nicht.", "Heutzutage gehen sie ins Kino nicht.",
                    "Heutzutage geht es ins Kino kein.", "Heutzutage geht es ins Kino nicht.",
                    "Heutzutage geht man ins Kino kein.", "Heutzutage geht man ins Kino nicht.",
                    "Heutzutage geht man nicht ins Kino.", "Heutzutage nicht geht man ins Kino."
                },
                "Heutzutage geht man ins Kino nicht."),
            new SplitOptionsTaskRoundInfo(
                "Дождь идет несколько часов.",
                "дождь - der Regen; идти- gehen / regnen (о дожде); несколько часов - seit einigen Stunden",
                new[]
                {
                    "Der Regen geht seit einigen Stunden.", "Der Regen ist seit einigen Stunden.",
                    "Der Regen regnet seit einigen Stunden.", "Der Regen seit einigen Stunden.",
                    "Es regnet seit einigen Stunden.", "Man regnet seit einigen Stunden.",
                    "Regnen seit einigen Stunden."
                },
                "Es regnet seit einigen Stunden."),
            new SplitOptionsTaskRoundInfo(
                "Гремит гром.",
                "гром - der Donner; греметь - klappern / donnern (о громе)",
                new[]
                {
                    "Der Donner donnert.", "Der Donner klappert.", "Donner.", "Donnern.", "Es donnert.",
                    "Es ist Donner.", "Man donnert."
                },
                "Es donnert."),
            new SplitOptionsTaskRoundInfo(
                "Молния не сверкает.",
                "молния - der Blitz; сверкать - schillern / blitzen (о молнии)",
                new[]
                {
                    "Der Blitz blitzt nicht.", "Der Blitz nicht blitzt.", "Der Blitz schillert nicht.",
                    "Es blitzt nicht.", "Es ist blitzen kein.", "Es ist kein blitzen.", "Es ist nicht Blitz.",
                    "Es ist nicht blitzen", "Es nicht blitzt.", "Kein Blitz.", "Nicht Blitz.", "Nicht blitzen."
                },
                "Es blitzt nicht."),
            new SplitOptionsTaskRoundInfo(
                "Это чай.",
                "чай - der Tee",
                new[]
                {
                    "Das Tee.", "Das der Tee.", "Das ein Tee.", "Das ist Tee.", "Das ist der Tee.", "Das ist ein Tee.",
                    "Der Tee.", "Ein Tee.", "Tee."
                },
                "Das ist Tee."),
            new SplitOptionsTaskRoundInfo(
                "Это всего лишь голод.",
                "всего лишь - nur; голод - der Hunger",
                new[]
                {
                    "Das ist nur Hunger.", "Das ist nur der Hunger.", "Das ist nur ein Hunger.", "Das nur Hunger.",
                    "Das nur der Hunger.", "Das nur ein Hunger.", "Nur Hunger.", "Nur der Hunger.", "Nur ein Hunger."
                },
                "Das ist nur Hunger."),
            new SplitOptionsTaskRoundInfo(
                "Это машина.",
                "машина - das Auto",
                new[]
                {
                    "Das Auto.", "Das ein Auto.", "Das ist Auto.", "Das ist das Auto.", "Das ist ein Auto.", "Ein Auto."
                },
                "Das ist ein Auto."),
            new SplitOptionsTaskRoundInfo(
                "Это господин Майер.",
                "господин Майер - der Herr Meier",
                new[]
                {
                    "Das Herr Meier.", "Das der Herr Meier.", "Das ein Herr Meier.", "Das ist Herr Meier.",
                    "Das ist der Herr Meier.", "Das ist ein Herr Meier.", "Herr Meier."
                },
                "Das ist Herr Meier."),
            new SplitOptionsTaskRoundInfo(
                "Это чай и кофе.",
                "чай - der Tee; кофе - der Kaffee",
                new[]
                {
                    "Das Tee und Kaffee.", "Das ein Tee und ein Kaffee.", "Das ist Tee und Kaffee.",
                    "Das ist ein Tee und ein Kaffee.", "Das sind Tee und Kaffee.", "Das sind der Tee und der Kaffee.",
                    "Das sind ein Tee und ein Kaffee.", "Der Tee und der Kaffee.", "Ein Tee und ein Kaffee.",
                    "Tee und Kaffee."
                },
                "Das sind Tee und Kaffee."),
            new SplitOptionsTaskRoundInfo(
                "Это мыши.",
                "мыши - die Mäuse",
                new[]
                {
                    "Das Mäuse.", "Das die Mäuse.", "Das eine Mäuse.", "Das ist Mäuse.", "Das sind Mäuse.",
                    "Das sind die Mäuse.", "Das sind eine Mäuse.", "Die Mäuse.", "Eine Mäuse.", "Mäuse."
                },
                "Das sind Mäuse."),
            new SplitOptionsTaskRoundInfo(
                "Это Эрика и Клара.",
                " ",
                new[]
                {
                    "Das Erika und Klara.", "Das eine Erika und eine Klara.", "Das ist Erika und Klara.",
                    "Das ist die Erika und die Klara.", "Das ist eine Erika und eine Klara.",
                    "Das sind Erika und Klara.", "Das sind die Erika und die Klara.",
                    "Das sind eine Erika und eine Klara.", "Die Erika und die Klara.", "Eine Erika und eine Klara.",
                    "Erika und Klara."
                },
                "Das sind Erika und Klara."),
            new SplitOptionsTaskRoundInfo(
                "Это не Эрика.",
                " ",
                new[]
                {
                    "Das ist Erika keine.", "Das ist Erika nicht.", "Das ist keine Erika.", "Das ist keine die Erika.",
                    "Das ist nicht Erika.", "Das ist nicht die Erika.", "Das keine Erika.", "Das nein Erika.",
                    "Das nicht Erika.", "Keine Erika.", "Nein Erika.", "Nicht Erika."
                },
                "Das ist nicht Erika."),
            new SplitOptionsTaskRoundInfo(
                "Это не машина.",
                "машина - das Auto",
                new[]
                {
                    "Das ist Auto nicht.", "Das ist ein Auto nicht.", "Das ist kein Auto.", "Das ist nicht Auto.",
                    "Das ist nicht ein Auto.", "Das kein Auto.", "Das nein Auto.", "Das nicht Auto.", "Kein Auto.",
                    "Nicht Auto.", "Nicht ein Auto."
                },
                "Das ist kein Auto."),
            new SplitOptionsTaskRoundInfo(
                "Это не голод.",
                "голод - der Hunger",
                new[]
                {
                    "Das Hunger kein.", "Das ein Hunger nicht.", "Das ist Hunger nicht.", "Das ist der Hunger nicht.",
                    "Das ist ein Hunger nicht.", "Das ist kein Hunger.", "Das ist nicht Hunger.",
                    "Das ist nicht der Hunger.", "Das ist nicht ein Hunger.", "Das kein Hunger.",
                    "Das nicht ein Hunger.", "Der Hunger nicht.", "Ein Hunger nicht.", "Hunger kein.", "Hunger nicht.",
                    "Kein Hunger.", "Nicht Hunger.", "Nicht der Hunger.", "Nicht ein Hunger."
                },
                "Das ist kein Hunger."),
            new SplitOptionsTaskRoundInfo(
                "Это не страхи.",
                "страхи - die Ängste",
                new[]
                {
                    "Das ist keine Ängste.", "Das ist nicht Ängste.", "Das ist Ängste nicht.", "Das keine Ängste.",
                    "Das nicht Ängste.", "Das sind keine Ängste.", "Das sind nicht keine Ängste.",
                    "Das sind nicht Ängste.", "Das sind Ängste nicht.", "Das Ängste nicht.", "Keine Ängste.",
                    "Nicht Ängste.", "Ängste nein."
                },
                "Das sind keine Ängste."),
            new SplitOptionsTaskRoundInfo(
                "Это не мыши.",
                "мыши - die Mäuse",
                new[]
                {
                    "Das Mäuse nicht.", "Das ist Mäuse keine.", "Das ist Mäuse nicht.", "Das ist keine Mäuse.",
                    "Das ist nicht Mäuse.", "Das keine Mäuse.", "Das sind Mäuse nicht.", "Das sind die Mäuse nicht.",
                    "Das sind keine Mäuse.", "Das sind nicht Mäuse.", "Das sind nicht die Mäuse.", "Keine Mäuse.",
                    "Nicht Mäuse."
                },
                "Das sind keine Mäuse."),
            new SplitOptionsTaskRoundInfo(
                "У тебя чай есть.",
                "у - bei; тебя - dir; чай - der Tee",
                new[]
                {
                    "Bei dir Tee.", "Bei dir haben Tee.", "Bei dir hast Tee.", "Bei dir ist Tee.", "Du Tee hast.",
                    "Du hast Tee."
                },
                "Du hast Tee."),
            new SplitOptionsTaskRoundInfo(
                "У нас  подарок есть.",
                "подарок - das Geschenk",
                new[]
                {
                    "Bei uns ein Geschenk haben.", "Bei uns ein Geschenk sein.", "Wir ein Geschenk haben.",
                    "Wir haben ein Geschenk."
                },
                "Wir haben ein Geschenk."),
            new SplitOptionsTaskRoundInfo(
                "Подарка у нас нет.",
                "подарок - das Geschenk",
                new[]
                {
                    "Bei uns hat ein Geschenk nicht.", "Bei uns hat kein Geschenk.", "Bei uns ist ein Geschenk nicht.",
                    "Bei uns ist kein Geschenk.", "Wir haben ein Geschenk nicht.", "Wir haben kein Geschenk.",
                    "Wir haben nicht ein Geschenk."
                },
                "Wir haben kein Geschenk."),
            new SplitOptionsTaskRoundInfo(
                "У тебя нет чая.",
                "у - bei; тебя - dir; чай - der Tee",
                new[]
                {
                    "Bei dir haben keinen Tee.", "Bei dir hast Tee nicht.", "Bei dir hast keinen Tee.",
                    "Bei dir ist ein Tee nicht.", "Bei dir ist kein Tee.", "Du hast einen Tee nicht.",
                    "Du hast keinen Tee.", "Du hast nicht Tee."
                },
                "Du hast keinen Tee."),
            new SplitOptionsTaskRoundInfo(
                "Здесь всегда есть чай.",
                "здесь - hier; всегда - immer; чай - der Tee",
                new[]
                {
                    "Hier gibt es immer Tee.", "Hier haben immer Tee.", "Hier haben sie immer Tee.",
                    "Hier hat man immer Tee.", "Hier immer Tee.", "Hier ist immer Tee.", "Hier sein immer Tee."
                },
                "Hier gibt es immer Tee."),
            new SplitOptionsTaskRoundInfo(
                "Здесь сейчас нет чая.",
                "здесь - hier; сейчас - jetzt; чай - der Tee",
                new[]
                {
                    "Hier gibt es jetzt Tee nicht.", "Hier gibt es jetzt keinen Tee.", "Hier gibt es jetzt nicht Tee.",
                    "Hier haben sie jetzt einen Tee nicht.", "Hier haben sie jetzt keinen Tee.",
                    "Hier ist jetzt Tee nicht.", "Hier ist jetzt kein Tee.", "Hier ist jetzt nicht Tee.",
                    "Hier jetzt Tee nein.", "Hier jetzt kein Tee.", "Hier jetzt keinen Tee.", "Hier jetzt nein Tee."
                },
                "Hier gibt es jetzt keinen Tee."),
            new SplitOptionsTaskRoundInfo(
                "Нам нужен чай.",
                "нам - uns; нужен - brauchen, nötig, müssen; чай - der Tee",
                new[]
                {
                    "Uns brauchen Tee.", "Uns braucht Tee.", "Uns ist nötig Tee.", "Uns muss Tee.", "Uns müssen Tee.",
                    "Uns nötig Tee.", "Wir brauchen Tee.", "Wir haben nötig Tee.", "Wir müssen Tee.", "Wir nötig Tee.",
                    "Wir sind nötig Tee."
                },
                "Wir brauchen Tee."),
            new SplitOptionsTaskRoundInfo(
                "Тебе нужна машина.",
                "тебе - dir; нужна /нужен - brauchen, nötig, müssen; машина - das Auto",
                new[]
                {
                    "Dir brauchen ein Auto.", "Dir brauchst ein Auto.", "Dir ist ein Auto nötig.",
                    "Dir ist nötig ein Auto.", "Dir muss ein Auto.", "Dir müssen ein Auto.", "Dir nötig ein Auto.",
                    "Du brauchst ein Auto.", "Du müssen ein Auto.", "Du nötig ein Auto.", "Wir haben nötig ein Auto.",
                    "Wir sind nötig ein Auto."
                },
                "Du brauchst ein Auto."),
            new SplitOptionsTaskRoundInfo(
                "Им не нужна машина.",
                "им - ihnen; нужен/нужна - brauchen, nötig, müssen; машина - das Auto",
                new[]
                {
                    "Ihnen brauchen ein Auto nicht.", "Ihnen brauchen kein Auto.", "Ihnen braucht ein Auto nicht.",
                    "Ihnen braucht kein Auto.", "Ihnen ist nicht nötig ein Auto.", "Ihnen ist nötig kein Auto.",
                    "Ihnen müssen kein Auto.", "Ihnen nicht muss ein Auto.", "Ihnen nicht müssen ein Auto.",
                    "Ihnen nicht nötig ein Auto.", "Sie brauchen ein Auto nicht.", "Sie brauchen kein Auto.",
                    "Sie sind nicht nötig ein Auto.", "Sie sind nicht nötig kein Auto."
                },
                "Sie brauchen kein Auto."),
            new SplitOptionsTaskRoundInfo(
                "Это важно.",
                "важно - wichtig",
                new[] { "Das wichtig.", "Es ist wichtig.", "Es wichtig.", "Wichtig." },
                "Es ist wichtig."),
            new SplitOptionsTaskRoundInfo(
                "Холодно.",
                "холодно - kalt",
                new[] { "Das kalt.", "Es ist kalt.", "Es kalt.", "Kalt." },
                "Es ist kalt."),
            new SplitOptionsTaskRoundInfo(
                "Не холодно.",
                "холодно - kalt",
                new[]
                {
                    "Das kalt nicht.", "Das nicht kalt.", "Es ist kalt kein.", "Es ist kalt nicht.",
                    "Es ist kein kalt.", "Es ist nicht kalt.", "Es nicht kalt.", "Kalt kein", "Kein kalt."
                },
                "Es ist nicht kalt."),
            new SplitOptionsTaskRoundInfo(
                "Слишком опасно плавать в этом озере.",
                "слишком - zu; опасно - gefährlich; плавать - schwimmen; в этом озере - in diesem See",
                new[]
                {
                    "Es ist zu gefährlich, in diesem See zu schwimmen.", "Es zu gefährlich in diesem See schwimmen.",
                    "Man schwimmen in diesem See zu gefährlich.", "Man schwimmt in diesem See zu gefährlich.",
                    "Schwimmen in diesem See zu gefährlich.", "Zu gefährlich in diesem See schwimmen.",
                    "Zu gefährlich schwimmen in diesem See.", "Zu gefährlich schwimmt man in diesem See."
                },
                "Es ist zu gefährlich, in diesem See zu schwimmen."),
            new SplitOptionsTaskRoundInfo(
                "Важно много ходить пешком.",
                "важно - wichtig; много - viel; ходить - laufen; пешком - zu Fuß",
                new[]
                {
                    "Es ist wichtig, viel zu Fuß zu laufen.", "Es wichtig viel zu Fuß laufen.",
                    "Man lauft viel zu Fuß wichtig.", "Wichtig viel zu Fuß laufen.", "Wichtig viel zu Fuß zu laufen."
                },
                "Es ist wichtig, viel zu Fuß zu laufen."),
            new SplitOptionsTaskRoundInfo(
                "Мало спать не слишком полезно для здоровья.",
                "мало - wenig; спать - schlafen; полезно - gesund; для здоровья - für die Gesundheit",
                new[]
                {
                    "Es ist nicht sehr gesund, wenig zu schlafen.", "Es kein gesund wenig schlafen.",
                    "Es nicht sehr gesund wenig schlafen.", "Nicht sehr gesund wenig schlafen.",
                    "Wenig schlafen nicht sehr gesund.", "Wenig schlaft man nicht gesund."
                },
                "Es ist nicht sehr gesund, wenig zu schlafen."),
            new SplitOptionsTaskRoundInfo(
                "Становится слишком опасно.",
                "становиться - sich stellen; слишком - zu; опасно - gefährlich",
                new[]
                {
                    "Es bekommt gefährlich.", "Es ist zu gefährlich.", "Es stellt sich zu gefährlich.",
                    "Es wird zu gefährlich.", "Es zu gefährlich.", "Zu gefährlich."
                },
                "Es wird zu gefährlich."),
            new SplitOptionsTaskRoundInfo(
                "Теплеет постепенно.",
                "теплеть - erwärmen; постепенно - langsam; тепло- warm",
                new[]
                {
                    "Es bekommt langsam warm.", "Es erwärmt langsam.", "Es ist langsam warm.", "Es wird langsam warm.",
                    "Langsam warm."
                },
                "Es wird langsam warm."),
            new SplitOptionsTaskRoundInfo(
                "Салат свежий.",
                "салат - der Salat; свежий - frisch",
                new[]
                {
                    "Der Salat frisch.", "Der Salat ist frisch.", "Der Salat sein frisch.", "Der frische Salat.",
                    "Frisch Salat."
                },
                "Der Salat ist frisch."),
            new SplitOptionsTaskRoundInfo(
                "Предложение очень трудное.",
                "предложение - der Satz; трудное - schwer; очень - sehr",
                new[]
                {
                    "Der Satz ist sehr schwer.", "Der Satz sehr schwer.", "Der Satz sein schwer.", "Sehr schwer Satz."
                },
                "Der Satz ist sehr schwer."),
            new SplitOptionsTaskRoundInfo(
                "Это мороженное из сливок.",
                "это мороженное - dieses Eis; из сливок - aus Sahne",
                new[]
                {
                    "Dieses Eis  sein aus Sahne.", "Dieses Eis Sahne.", "Dieses Eis aus Sahne.",
                    "Dieses Eis ist aus Sahne."
                },
                "Dieses Eis ist aus Sahne."),
            new SplitOptionsTaskRoundInfo(
                "Мороженное слишком холодное, чтобы его сразу есть.",
                "мороженное - das Eis; слишком - zu; холодное - kalt; сразу - gleich; его - es; есть - essen",
                new[]
                {
                    "Das Eis essen gleich zu kalt.", "Das Eis isst gleich zu kalt.", "Das Eis isst man gleich zu kalt.",
                    "Das Eis ist zu kalt, es gleich essen.", "Das Eis ist zu kalt, es gleich zu essen.",
                    "Das Eis zu kalt gleich essen."
                },
                "Das Eis ist zu kalt, es gleich zu essen."),
            new SplitOptionsTaskRoundInfo(
                "Предложение слишком трудное, чтобы его перевести без словаря.",
                "предложение - der Satz; трудное - schwer; слишком - zu; переводить - übersetzen; без словаря - ohne Wörterbuch; его - ihn ",
                new[]
                {
                    "Den Satz übersetzen ohne Wörterbuch zu schwer.",
                    "Den Satz übersetzt man ohne Wörterbuch zu schwer.",
                    "Der Satz ist zu schwer ohne Wörterbuch übersetzen.",
                    "Der Satz ist zu schwer, ihn ohne Wörterbuch zu übersetzen.",
                    "Der Satz ohne Wörterbuch übersetzen zu schwer.",
                    "Der Satz zu schwer ihn ohne Wörterbuch übersetzen.",
                    "Der Satz zu schwer sein ohne Wörterbuch übersetzen.",
                    "Der Satz übersetzen ohne Wörterbuch zu schwer."
                },
                "Der Satz ist zu schwer, ihn ohne Wörterbuch zu übersetzen."),
            new SplitOptionsTaskRoundInfo(
                "Погода портится.",
                "погода - das Wetter; портиться - verderben; плохой - schlecht",
                new[]
                {
                    "Das Wetter bekommt schlecht.", "Das Wetter ist schlecht.", "Das Wetter schlecht.",
                    "Das Wetter verdirbt schlecht.", "Das Wetter verdirbt.", "Das Wetter wird schlecht."
                },
                "Das Wetter wird schlecht."),
            new SplitOptionsTaskRoundInfo(
                "Фильм становится скучным.",
                "фильм - der Film; становиться - sich stellen; скучный - langweilig",
                new[]
                {
                    "Der Film bekommt langweilig.", "Der Film ist langweilig.", "Der Film langweilig.",
                    "Der Film stellt sich langweilig.", "Der Film wird langweilig."
                },
                "Der Film wird langweilig.")
        };

        public SplitOptionsTask() : base(
            8,
            "Разделите варианты написания фразы",
            "Перетащите варианты перевода в стоблцы.",
            RoundInfos)
        //RoundInfos.Take(10).ToArray())
        {
            IsSounded = false;
        }

        public override bool OptionIsCorrect(string option)
        {
            return ((SplitOptionsTaskRoundInfo)CurrentRoundInfo).AllOptionsAreSet;
        }

        public string CurrentRoundNotes => ((SplitOptionsTaskRoundInfo)CurrentRoundInfo).Notes;
    }
}