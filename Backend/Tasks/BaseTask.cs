﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Backend.Tasks
{
    public class BaseTask
    {
        private readonly IEnumerable<BaseTaskRoundInfo> _permutedRoundInfos;
        private readonly BaseTaskRoundInfo[] _sourceRoundInfos;
        public readonly string Description;
        public readonly int Id;
        public readonly string Name;

        protected BaseTask(int id, string name, string description, BaseTaskRoundInfo[] sourceRoundInfos)
        {
            Id = id;
            Name = name;
            Description = description;
            CurrentRoundNumber = 0;

            sourceRoundInfos = sourceRoundInfos.ToArray();
            _sourceRoundInfos = sourceRoundInfos;
            _permutedRoundInfos = Permutator.Permute(_sourceRoundInfos).Cast<BaseTaskRoundInfo>();
            NextRound();
        }

        protected BaseTaskRoundInfo CurrentRoundInfo => _permutedRoundInfos.ElementAt(CurrentRoundNumber - 1);
        public string RoundPhrase => CurrentRoundInfo.Phrase;
        public List<string> RoundOptions { get; private set; }
        public string RightOption => CurrentRoundInfo.RightOption;

        public int TotalRoundsNumber => _sourceRoundInfos.Length;
        public int CurrentRoundNumber { get; private set; }
        public int CurrentRoundIndex => Array.IndexOf(_sourceRoundInfos, CurrentRoundInfo);
        public bool IsComplete => CurrentRoundNumber > TotalRoundsNumber;
        public bool IsSounded { get; protected set; } = true;

        public virtual bool OptionIsCorrect(string option)
        {
            return option == RightOption;
        }

        public void NextRound()
        {
            CurrentRoundNumber++;
            if (IsComplete)
                return;
            RoundOptions = Permutator.Permute(CurrentRoundInfo.Options).Cast<string>().ToList();
        }
    }
}