﻿namespace Backend.Tasks.SelectRightAnswer
{
    public class TaskRoundInfoWithTranslation : BaseTaskRoundInfo
    {
        public TaskRoundInfoWithTranslation(
            string phrase,
            string phraseTranslation,
            string[] options,
            string rightOption) : base(phrase, options, rightOption)
        {
            PhraseTranslation = phraseTranslation;
        }

        public string PhraseTranslation { get; }
    }
}