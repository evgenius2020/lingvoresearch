﻿namespace Backend.Tasks.SelectRightAnswer
{
    public class TranslateRussianToGermanTask : BaseTask
    {
        public static readonly BaseTaskRoundInfo[] RoundInfos =
        {
            new BaseTaskRoundInfo("гарнир", new[] {"die Beilage", "der Nachtisch", "die Rechnung", "der Braten"},
                "die Beilage"),
            new BaseTaskRoundInfo("десерт", new[] {"die Beilage", "der Nachtisch", "die Rechnung", "der Braten"},
                "der Nachtisch"),
            new BaseTaskRoundInfo("быть на вкус", new[] {"bestellen", "schmecken", "bezahlen", "mögen"},
                "schmecken"),
            new BaseTaskRoundInfo("заказывать", new[] {"bestellen", "schmecken", "bezahlen", "mögen"},
                "bestellen"),
            new BaseTaskRoundInfo("счет", new[] {"die Beilage", "der Nachtisch", "die Rechnung", "der Braten"},
                "die Rechnung"),

            new BaseTaskRoundInfo("жаркое", new[] {"die Beilage", "der Nachtisch", "die Rechnung", "der Braten"},
                "der Braten"),
            new BaseTaskRoundInfo("оплачивать", new[] {"bezahlen", "nehmen", "mögen", "bestellen"}, "bezahlen"),
            new BaseTaskRoundInfo("брать", new[] {"bezahen", "nehmen", "mögen", "bestellen"}, "nehmen"),
            new BaseTaskRoundInfo("любить", new[] {"bezahen", "nehmen", "mögen", "bestellen"}, "mögen"),
            new BaseTaskRoundInfo("сытый", new[] {"satt", "salzig", "scharf", "fett"}, "satt"),

            new BaseTaskRoundInfo("солёный", new[] {"satt", "salzig", "scharf", "fett"}, "salzig"),
            new BaseTaskRoundInfo("острый", new[] {"satt", "salzig", "scharf", "fett"}, "scharf"),
            new BaseTaskRoundInfo("жирный", new[] {"satt", "salzig", "scharf", "fett"}, "fett"),
            new BaseTaskRoundInfo("сладкий", new[] {"süß", "sauer", "satt", "fett"}, "süß"),
            new BaseTaskRoundInfo("кислый", new[] {"süß", "sauer", "satt", "fett"}, "sauer"),

            new BaseTaskRoundInfo("закуска", new[] {"Vorspeise", "Hauptgang", "Brötchen", "belegtes Brot"},
                "Vorspeise"),
            new BaseTaskRoundInfo("основное меню", new[] {"Vorspeise", "Hauptgang", "Brötchen", "belegtes Brot"},
                "Hauptgang"),
            new BaseTaskRoundInfo("булочка",
                new[] {"Vorspeise", "Hauptgang", "Brötchen", "belegtes Brot"}, "Brötchen"),
            new BaseTaskRoundInfo("бутерброд", new[] {"Brot", "belegtes Brötchen", "Vorspeise", "Hauptgang"},
                "belegtes Brötchen"),
            new BaseTaskRoundInfo("бутерброд с сыром", new[] {"Käsebrot", "Wurstbrot", "Volkornbrot", "Brathähnchen"},
                "Käsebrot"),

            new BaseTaskRoundInfo("бутерброд с колбасой",
                new[] {"Käsebrot", "Wurstbrot", "Volkornbrot", "Brathähnchen"}, "Wurstbrot"),
            new BaseTaskRoundInfo("цельнозерновой хлеб",
                new[] {"Käsebrot", "Wurstbrot", "Vollkornbrot", "Brathähnchen"},
                "Vollkornbrot"),
            new BaseTaskRoundInfo("жаренная курочка", new[] {"Käsebrot", "Wurstbrot", "Volkornbrot", "Brathähnchen"},
                "Brathähnchen"),
            new BaseTaskRoundInfo("сырное ассорти",
                new[] {"Käseteller", "Rindfeischsuppe", "Schwarzbrot", "Bratwürstchen"}, "Käseteller"),
            new BaseTaskRoundInfo("бульон из говядины",
                new[] {"Käseteller", "Rindfeischsuppe", "Schwarzbrot", "Bratwürstchen"}, "Rindfeischsuppe"),
            
            new BaseTaskRoundInfo("черный хлеб",
                new[] {"Käseteller", "Rindfeischsuppe", "Schwarzbrot", "Bratwürstchen"}, "Schwarzbrot"),
            new BaseTaskRoundInfo("сарделька", new[] {"Käseteller", "Rindfeischsuppe", "Schwarzbrot", "Bratwürstchen"},
                "Bratwürstchen"),
            new BaseTaskRoundInfo("краснокочанная капуста", new[] {"Rotkohl", "Schlagsahne", "Gericht", "Teller"},
                "Rotkohl"),
            new BaseTaskRoundInfo("взбитые сливки", new[] {"Rotkohl", "Schlagsahne", "Gericht", "Teller"},
                "Schlagsahne"),
            new BaseTaskRoundInfo("блюдо", new[] {"Rotkohl", "Schlagsahne", "Gericht", "Teller"}, "Gericht")
        };

        public TranslateRussianToGermanTask() : base(
            2,
            "Выберите правильный перевод (рус-нем)",
            "Нажмите мышкой нужное слово",
            RoundInfos)
        {
        }
    }
}