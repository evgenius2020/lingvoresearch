﻿namespace Backend.Tasks.SelectRightAnswer
{
    public class TranslateGermanToRussianTask : BaseTask
    {
        public static readonly BaseTaskRoundInfo[] RoundInfos =
        {
            new BaseTaskRoundInfo("der Schinken",
                new[] {"колбаса", "ветчина", "приправа", "бутерброд"}, "ветчина"),
            new BaseTaskRoundInfo("die Wurst",
                new[] {"колбаса", "ветчина", "приправа", "бутерброд"}, "колбаса"),
            new BaseTaskRoundInfo("der Käseteller",
                new[] {"сырное ассорти", "ветчина", "приправа", "бутерброд"}, "сырное ассорти"),
            new BaseTaskRoundInfo("die Bratkartoffeln",
                new[] {"колбаса", "ветчина", "приправа", "жареный картофель"}, "жареный картофель"),
            new BaseTaskRoundInfo("das Gewürze",
                new[] {"колбаса", "ветчина", "приправа", "бутерброд"}, "приправа"),
            
            new BaseTaskRoundInfo("belegtes Brötchen",
                new[] {"колбаса", "ветчина", "приправа", "бутерброд"}, "бутерброд"),
            new BaseTaskRoundInfo("Gericht",
                new[] {"блюдо", "напиток", "приправа", "овощи"}, "блюдо"),
            new BaseTaskRoundInfo("Brathähnchen",
                new[] {"жареная курочка", "овощной суп", "рыбное ассорти", "рыбный суп"}, "жареная курочка"),
            new BaseTaskRoundInfo("Gemüsesuppe",
                new[] {"жареная курочка", "овощной суп", "рыбное ассорти", "рыбный суп"}, "овощной суп"),
            new BaseTaskRoundInfo("Fischplatte",
                new[] {"жареная курочка", "овощной суп", "рыбное ассорти", "рыбный суп"}, "рыбное ассорти"),
            
            new BaseTaskRoundInfo("Fischsuppe",
                new[] {"жареная курочка", "овощной суп", "рыбное ассорти", "рыбный суп"}, "рыбный суп"),
            new BaseTaskRoundInfo("Sahne",
                new[] {"сливки", "взбитые сливки", "сахар", "перец"}, "сливки"),
            new BaseTaskRoundInfo("Schlagsahne",
                new[] {"сливки", "взбитые сливки", "сахар", "перец"}, "взбитые сливки"),
            new BaseTaskRoundInfo("Zucker",
                new[] {"сливки", "взбитые сливки", "сахар", "перец"}, "сахар"),
            new BaseTaskRoundInfo("Pfeffer",
                new[] {"сливки", "взбитые сливки", "сахар", "перец"}, "перец"),
            
            new BaseTaskRoundInfo("Salz",
                new[] {"соль", "апельсиновый сок", "овощной салат", "бутерброд с ветчиной"}, "соль"),
            new BaseTaskRoundInfo("Orangensaft",
                new[] {"соль", "апельсиновый сок", "овощной салат", "бутерброд с ветчиной"}, "апельсиновый сок"),
            new BaseTaskRoundInfo("Gemüsesalat",
                new[] {"соль", "апельсиновый сок", "овощной салат", "бутерброд с ветчиной"}, "овощной салат"),
            new BaseTaskRoundInfo("Schinkenbrot",
                new[] {"соль", "апельсиновый сок", "овощной салат", "бутерброд с ветчиной"}, "бутерброд с ветчиной"),
            new BaseTaskRoundInfo("Käsebrot",
                new[] {"бутерброд с сыром", "бутерброд с колбасой", "цельнозерновой хлеб", "черный хлеб"},
                "бутерброд с сыром"),
            
            new BaseTaskRoundInfo("Wurstbrot",
                new[] {"бутерброд с сыром", "бутерброд с колбасой", "цельнозерновой хлеб", "черный хлеб"},
                "бутерброд с колбасой"),
            new BaseTaskRoundInfo("Vollkornbrot",
                new[] {"бутерброд с сыром", "бутерброд с колбасой", "цельнозерновой хлеб", "черный хлеб"},
                "цельнозерновой хлеб"),
            new BaseTaskRoundInfo("Schwarzbrot",
                new[] {"бутерброд с сыром", "бутерброд с колбасой", "цельнозерновой хлеб", "черный хлеб"},
                "черный хлеб"),
            new BaseTaskRoundInfo("Bratwürstchen",
                new[] {"сарделька", "мороженое", "пирог", "краснокочанная капуста"},
                "сарделька"),
            new BaseTaskRoundInfo("Eis",
                new[] {"сарделька", "мороженое", "пирог", "краснокочанная капуста"},
                "мороженое"),
            
            new BaseTaskRoundInfo("Kuchen",
                new[] {"сарделька", "мороженое", "пирог", "краснокочанная капуста"},
                "пирог"),
            new BaseTaskRoundInfo("Rotkohl",
                new[] {"сарделька", "мороженое", "пирог", "краснокочанная капуста"},
                "краснокочанная капуста"),
            new BaseTaskRoundInfo("bestellen",
                new[] {"заказывать", "быть на вкус", "сытый", "любить"},
                "заказывать"),
            new BaseTaskRoundInfo("schmecken",
                new[] {"заказывать", "быть на вкус", "сытый", "любить"},
                "быть на вкус"),
            new BaseTaskRoundInfo("satt",
                new[] {"заказывать", "быть на вкус", "сытый", "любить"},
                "сытый"),
        };

        public TranslateGermanToRussianTask() : base(
            3,
            "Выберите правильный перевод (нем-рус)",
            "Нажмите мышкой нужное слово",
            RoundInfos)
        {
        }
    }
}