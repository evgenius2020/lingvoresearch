﻿namespace Backend.Tasks.SelectRightAnswer
{
    public class SelectRightAnswerTask : BaseTask
    {
        public static readonly BaseTaskRoundInfo[] RoundInfos =
        {
            new BaseTaskRoundInfo("Ich trinke gern ….",
                new[] {"Tee", "Apfel", "Gabel", "Tasse"}, "Tee"),
            new BaseTaskRoundInfo("Ich nehme 2 … Saft.",
                new[] {"Gläser", "Glas", "Glasses", "Gläsern"}, "Glas"),
            new BaseTaskRoundInfo("Geht das … oder zusammen?",
                new[] {"per Karte", "extra", "einzeln", "getrennt"}, "getrennt"),
            new BaseTaskRoundInfo("Der Braten … mir zu scharf.",
                new[] {"bestellt", "bezahlt", "schmeckt", "macht"}, "schmeckt"),
            new BaseTaskRoundInfo("Möchtest du Reis als …?",
                new[] {"Bezahlung", "Getränke", "Nachtisch", "Beilage"}, "Beilage"),

            new BaseTaskRoundInfo("Was nehmen Sie … Vorspeise?", 
                new[] {"als", "wie", "per", "für"}, "als"),
            new BaseTaskRoundInfo("Zum Kaffee nehmen wir etwas vom ….",
                new[] {"Vorspeise", "Nachtisch", "Brot", "Butter"}, "Nachtisch"),
            new BaseTaskRoundInfo("Ich mag lieber Kaffee … Tee.", 
                new[] {"wie", "per", "für", "als"}, "als"),
            new BaseTaskRoundInfo("Belegte … ohne Grünsalat sind nicht so gesund.",
                new[] {"Brote", "Kuchen", "Fisch", "Schinken"}, "Brote"),
            new BaseTaskRoundInfo("Was … du als Nachtisch?", 
                new[] {"machst", "bestellst", "trinkst", "schmeckst"}, "bestellst"),
            
            new BaseTaskRoundInfo("… Fisch hier empfehle ich.", 
                new[] {"Die", "Der", "Das", "Den"}, "Den"),
            new BaseTaskRoundInfo("Der … schmeckt zu sauer.", 
                new[] {"Reis", "Butter", "Orangensaft", "Limonade"}, "Orangensaft"),
            new BaseTaskRoundInfo("Trinken Sie … Kaffee?", 
                new[] {"ein", "einer", "einen", "eine"}, "einen"),
            new BaseTaskRoundInfo("Haben Sie …wasser?", 
                new[] {"Still-", "Vor-", "Laut-", "Wurst-"}, "Still-"),
            new BaseTaskRoundInfo("Heiße … im Sommer machen mich nicht frischt.",
                new[] {"Kaffee", "Tee", "Getränke", "Milch"}, "Getränke"),
            
            new BaseTaskRoundInfo("Zwei Flaschen … bitte.", 
                new[] {"des Biers", "Biers", "Bieres", "Bier"}, "Bier"),
            new BaseTaskRoundInfo("Als … nehme ich einen Käseteller.",
                new[] {"Suppe", "Vorspeise", "Beilage", "Getränk"}, "Vorspeise"),
            new BaseTaskRoundInfo("Schinken und Wurst passen gut zum ….",
                new[] {"Frühstück", "Beilage", "Vorspeise", "Nachtisch"}, "Frühstück"),
            new BaseTaskRoundInfo("Bratkartoffeln passen gut als ….",
                new[] {"Getränke", "Speisekarte", "Beilage", "Vorspeise"}, "Beilage"),
            new BaseTaskRoundInfo("Rindfleisch ist weniger … als Schweinefleisch.",
                new[] {"fett", "gewürzt", "arm", "süß"}, "fett"),
            
            new BaseTaskRoundInfo("Was isst du gewöhnlich … Mittagessen?", 
                new[] {"per", "für", "zum", "mit"}, "zum"),
            new BaseTaskRoundInfo("Welche … gibt es in diesem Salat?", 
                new[] {"Salz", "Zucker", "Gewürze", "Pfeffer"}, "Gewürze"),
            new BaseTaskRoundInfo("Und wie … dir der Kartoffelnsalat?",
                new[] {"macht", "nimmt", "bestellt", "schmeckt"}, "schmeckt"),
            new BaseTaskRoundInfo("Wir möchten …, die Rechnung bitte!",
                new[] {"nehmen", "bezahlen", "bestellen", "trinken"}, "bezahlen"),
            new BaseTaskRoundInfo("Ein Glas Wein, Obstsalat, das … 12 Euro.",
                new[] {"nimmt", "macht", "bestellt", "bezahlt"}, "macht"),
            
            new BaseTaskRoundInfo("Ich muss 11 Euro 10 Cent bezahlen, hier 12 Euro, … so.",
                new[] {"bestellt", "stimmt", "schmeckt", "macht"}, "stimmt"),
            new BaseTaskRoundInfo("Möchten Sie … oder mit der Karte bezahlen?",
                new[] {"bar", "scharf", "fett", "sauer"}, "bar"),
            new BaseTaskRoundInfo("Kann ich … Karte bezahlen?", 
                new[] {"als", "für", "mit", "per"}, "per"),
            new BaseTaskRoundInfo("Als Nachtisch möchte ich bitte ein Stück …, aber bitte ohne Schlagsahne.",
                new[] {"Kuchen", "Kuchens", "Kuchene", "Kuchenes"}, "Kuchen"),
            new BaseTaskRoundInfo("Zum Trinken nehme ich … Cola.", 
                new[] {"eine", "ein", "einen", "eins"}, "eine")
        };


        public SelectRightAnswerTask() : base(
            1,
            "Выберите правильный ответ",
            "Перетащите мышкой пропущенное слово",
            RoundInfos)
        {
        }
    }
}