﻿namespace Backend.Tasks
{
    public class BaseTaskRoundInfo
    {
        public BaseTaskRoundInfo(string phrase, string[] options, string rightOption)
        {
            Phrase = phrase;
            Options = options;
            RightOption = rightOption;
        }

        public string Phrase { get; }
        public string[] Options { get; }
        public string RightOption { get; }
    }
}