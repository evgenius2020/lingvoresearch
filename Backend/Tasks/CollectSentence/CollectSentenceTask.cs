﻿namespace Backend.Tasks.CollectSentence
{
    public class CollectSentenceTask : BaseTask
    {
        public static readonly BaseTaskRoundInfo[] RoundInfos =
        {
            new CollectSentenceTaskRoundInfo("Что будете заказывать?", "Was möchten Sie bestellen?"),
            new CollectSentenceTaskRoundInfo("Кофе слишком крепкий.", "Der Kaffee ist zu stark."),
            new CollectSentenceTaskRoundInfo("Мне кусочек пирога, пожалуйста.",
                "Ich nehme ein Stück Kuchen bitte."),
            new CollectSentenceTaskRoundInfo("Вместе считать или раздельно?", "Geht es getrennt oder zusammen?"),
            new CollectSentenceTaskRoundInfo("Это стоит 12 евро.", "Das macht 12 Euro."),
            
            new CollectSentenceTaskRoundInfo("Счет, пожалуйста.", "Die Rechnung bitte."),
            new CollectSentenceTaskRoundInfo("Что ты будешь на гарнир?", "Was nimmst du als Beilage?"),
            new CollectSentenceTaskRoundInfo("На десерт мы будем фруктовое ассорти.",
                "Als Nachtisch bestellen wir einen Obstteller."),
            new CollectSentenceTaskRoundInfo("Сырное ассорти хорошо подойдет к вину.",
                "Der Käseteller passt gut zum Wein."),
            new CollectSentenceTaskRoundInfo("Мне, пожалуйста, жаркое, но с овощами.",
                "Ich möchte bitte einen Braten, aber mit Gemüse bitte."),
            
            new CollectSentenceTaskRoundInfo("Какие специи в салате?", "Was für Gewürze haben Sie im Salat?"),
            new CollectSentenceTaskRoundInfo("Рыба невкусная, слишком соленая.",
                "Der Fisch schmeckt mir nicht, er ist zu salzig."),
            new CollectSentenceTaskRoundInfo("У мороженного вкус клубники.", "Das Eis schmeckt nach Erdbeere."),
            new CollectSentenceTaskRoundInfo("Нам три стакана сока.", "Wir nehmen drei Glas Saft."),
            new CollectSentenceTaskRoundInfo("К мясу я возьму бокал красного вина.",
                "Zum Fleisch nehme ich ein Glas Rotwein."),
            
            new CollectSentenceTaskRoundInfo("У вас есть горячие закуски?", "Haben Sie warme Vorspeise?"),
            new CollectSentenceTaskRoundInfo("Как тебе жаркое?", "Wie schmeckt dir der Braten?"),
            new CollectSentenceTaskRoundInfo("После обеда я обычно не пью кофе.",
                "Nachmittags trinke ich keinen Kaffee."),
            new CollectSentenceTaskRoundInfo("На десерт я хочу мороженное, но без взбитых сливок, пожалуйста.",
                "Als Nachtisch nehme ich ein Eis, aber ohne Schlagsahne bitte."),
            new CollectSentenceTaskRoundInfo("Суп на мой вкус слишком острый.", "Die Suppe schmeckt mir zu scharf."),
            
            new CollectSentenceTaskRoundInfo("На завтрак я пью только стакан апельсинового сока.",
                "Zum Frühstück trinke ich immer nur ein Glas Orangensaft."),
            new CollectSentenceTaskRoundInfo("Без сдачи.", "Stimmt so."),
            new CollectSentenceTaskRoundInfo("Ты будешь на гарнир красную капусту или жареный картофель?",
                "Nimmst du als Beilage Rotkohl oder Bratkartoffeln?"),
            new CollectSentenceTaskRoundInfo("Я не ем рыбу.", "Ich esse keinen Fisch."),
            new CollectSentenceTaskRoundInfo("Ты больше любишь чай или кофе?", "Trinkst du lieber Tee oder Kaffee?"),
            
            new CollectSentenceTaskRoundInfo("Булочка с маслом и джемом - это мой завтрак.",
                "Ein Brötchen mit Butter und Marmelade ist mein Frühstück."),
            new CollectSentenceTaskRoundInfo("Что из напитков будете?", "Was nehmen Sie von Getränken?"),
            new CollectSentenceTaskRoundInfo("На ужин я иногда ем овощной суп.",
                "Zum Abendbrot esse ich manchmal eine Gemüsesuppe."),
            new CollectSentenceTaskRoundInfo("Пирог не слишком сладкий на твой вкус?",
                "Schmeckt dir der Kuchen nicht zu süß?"),
            new CollectSentenceTaskRoundInfo("Он не любит мясо и овощи.", "Er mag kein Fleisch und kein Gemüse.")
        };

        public CollectSentenceTask() : base(
            7,
            "Перевод на немецкий",
            "Расположите слова в правильном порядке при помощи перевода",
            RoundInfos)
        {
        }

        public string RoundSentenceTranslation =>
            ((CollectSentenceTaskRoundInfo) CurrentRoundInfo).SentenceTranslation;
    }
}