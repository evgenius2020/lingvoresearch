﻿using System.Collections.Generic;
using System.Linq;

namespace Backend.Tasks.CollectSentence
{
    public class CollectSentenceTaskRoundInfo : BaseTaskRoundInfo
    {
        public CollectSentenceTaskRoundInfo(string sentenceTranslation, string phrase) :
            base(phrase, SimplifyString(phrase).Split(' '), SimplifyString(phrase))
        {
            SentenceTranslation = sentenceTranslation;
        }

        public string SentenceTranslation { get; }

        private static string RemoveAllEntries(string s, IEnumerable<string> templates)
        {
            return templates.Aggregate(s, (current, template) => current.Replace(template, string.Empty));
        }

        private static string SimplifyString(string s)
        {
            return RemoveAllEntries(s.ToLower(), new[] {".", ",", "!", "?"});
        }
    }
}